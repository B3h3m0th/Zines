
                  +-------------------------------+
                  |   *        Hackers       *    |
                  |    (a short bibliography)     |
                  |      journals/magazines       |
                  |                               |
                  |            12/20/92           |
                  |          [MOLE05.TXT]         |
                  +-------------------------------+

Search request: FI TW HACKERS 
Search result:  10 citations in the CC article database


1. IGNATIN GR.
     LET THE HACKERS HACK - ALLOWING THE REVERSE ENGINEERING OF COPYRIGHTED
   COMPUTER PROGRAMS TO ACHIEVE COMPATIBILITY.
     UNIVERSITY OF PENNSYLVANIA LAW REVIEW, 1992 MAY, V140 N5:1999-2050.

2. CHARLES D.
     INNOCENT HACKERS WANT THEIR COMPUTERS BACK.
     Pub type:  Editorial.
     NEW SCIENTIST, 1992 MAY 9, V134 N1820:9-9.

3. MOSLEY W.
     CYBERPUNK - OUTLAWS AND HACKERS ON THE COMPUTER FRONTIER - HAFNER,K,
   MARKOFF,J.
     Pub type:  Book Review.
     NEW YORK TIMES BOOK REVIEW, 1991 AUG 11,  AUG:15-15.

4. HACKERS FACE STRICTER CONTROLS.
     Pub type:  Editorial.
     ELECTRONICS WORLD & WIRELESS WORLD, 1991 JUL, V97 N1665:545-545.

5. GEAKE E.
     VOICE-SPOTTING COMPUTER CATCHES HACKERS.
     Pub type:  Editorial.
     NEW SCIENTIST, 1991 JUN 15, V130 N1773:28-28.

6. SOBCHACK V.
     NEW-AGE MUTANT NINJA HACKERS (THE CYBERSPACE OF VIRTUAL-REALITY (VR))
     Pub type:  Note.
     ARTFORUM, 1991 APR, V29 N8:24-26.

7. SPAFFORD E.
     ON HIRING HACKERS.
     Pub type:  Letter.
     COMMUNICATIONS OF THE ACM, 1990 OCT, V33 N10:14-14.

8. SHULMAN S.
     COMPUTERS - HACKERS GENIUS AWARD.
     Pub type:  Editorial.
     NATURE, 1990 JUL 19, V346 N6281:209-209.

9. DICKMAN S.
     COMPUTER SECURITY - KGB HACKERS GET OFF LIGHTLY.
     Pub type:  Editorial.
     NATURE, 1990 MAR 1, V344 N6261:6-6.

10. LINDLEY D.
      COMPUTER SECURITY - HACKERS INTENTIONS KEY TO COURT CASE.
      Pub type:  Editorial.
      NATURE, 1989 AUG 3, V340 N6232:329-329.




CC-> 


X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X
 Another file downloaded from:                                NIRVANAnet(tm)

 &TOTSE                510/935-5845   Walnut Creek, CA         Taipan Enigma
 Burn This Flag        408/363-9766       San Jose, CA                Zardoz
 realitycheck          415/666-0339  San Francisco, CA    Poindexter Fortran
 Governed Anarchy      510/226-6656        Fremont, CA             Eightball
 New Dork Sublime      805/823-1346      Tehachapi, CA               Biffnix
 Lies Unlimited        801/278-2699 Salt Lake City, UT            Mick Freen
 Atomic Books          410/669-4179      Baltimore, MD               Baywolf
 Sea of Noise          203/886-1441        Norwich, CT             Mr. Noise
 The Dojo              713/997-6351       Pearland, TX               Yojimbo
 Frayed Ends of Sanity 503/965-6747     Cloverdale, OR              Flatline
 The Ether Room        510/228-1146       Martinez, CA Tiny Little Super Guy
 Hacker Heaven         860/456-9266        Lebanon, CT         The Visionary
 The Shaven Yak        510/672-6570        Clayton, CA             Magic Man
 El Observador         408/372-9054        Salinas, CA         El Observador
 Cool Beans!           415/648-7865  San Francisco, CA        G.A. Ellsworth
 DUSK Til Dawn         604/746-5383   Cowichan Bay, BC         Cyber Trollis
 The Great Abyss       510/482-5813        Oakland, CA             Keymaster

                          "Raw Data for Raw Nerves"
X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X
