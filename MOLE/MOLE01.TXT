
                  +-------------------------------+
                  |   *    Computer Crime    *    |
                  |    (a short bibliography)     |
                  |      journals/magazines       |
                  |                               |
                  |            12/20/92           |
                  |          [MOLE01.TXT]         |
                  +-------------------------------+

Search request: FI TW COMPUTER CRIME
Search result:  14 citations in the CC article database


1. COLLIER PA; SPAUL BJ.
     A FORENSIC METHODOLOGY FOR COUNTERING COMPUTER CRIME.
     ARTIFICIAL INTELLIGENCE REVIEW, 1992, V6 N2:203-215.

2. CASTILLO J; DOYLE B; DUBEY S.
     COMPUTER CRIME.
     AMERICAN CRIMINAL LAW REVIEW, 1992 WINTER, V29 N2:221-241.

3. COLLIER P; SPAUL B.
     FORENSIC SCIENCE AGAINST COMPUTER CRIME IN THE UNITED-KINGDOM.
     Pub type:  Note.
     JOURNAL OF THE FORENSIC SCIENCE SOCIETY, 1992 JAN-MAR, V32 N1:27-34.

4. SULLIVAN GR.
     CRIME AND THE COMPUTER - WASIK,M.
     Pub type:  Book Review.
     CRIMINAL LAW REVIEW, 1992 FEB,  FEB:138-138.

5. FREEMAN L; MYKYTYN PP.
     COMPUTER CRIME AND DATA COPYRIGHT - ISSUES FOR IS MANAGEMENT .2.
     Pub type:  Editorial.
     JOURNAL OF SYSTEMS MANAGEMENT, 1991 DEC, V42 N12:18-18.

6. FREEMAN L; MYKYTYN PP.
     COMPUTER CRIME AND DATA COPYRIGHT - ISSUES FOR IS MANAGEMENT.
     JOURNAL OF SYSTEMS MANAGEMENT, 1991 NOV, V42 N11:18-18.

7. NICHOLSON CK; CUNNINGHAM R.
     COMPUTER CRIME.
     AMERICAN CRIMINAL LAW REVIEW, 1991, V28 N3:393-405.

8. MICHALOWSKI RJ; PFUHL EH.
     TECHNOLOGY, PROPERTY, AND LAW - THE CASE OF COMPUTER CRIME.
     CRIME LAW AND SOCIAL CHANGE, 1991 MAY, V15 N3:255-275.

9. ANDERSON C.
     COMPUTER SECURITY - NETWORKS SPUR TO CRIME.
     Pub type:  Editorial.
     NATURE, 1990 DEC 13, V348 N6302:574-574.

10. POWERS AC.
      FIGHTING CRIME WITH COMPUTER NETWORKS.
      JOURNAL OF SYSTEMS MANAGEMENT, 1990 OCT, V41 N10:24+.

11. ROSENBLATT K.
      DETERRING COMPUTER CRIME.
      TECHNOLOGY REVIEW, 1990 FEB-MAR, V93 N2:34-40.

12. ANDERSON GC.
      COMPUTER CRIME - CORNELL HACKER IS CONVICTED BUT FREE.
      Pub type:  Editorial.
      NATURE, 1990 FEB 1, V343 N6257:398-398.

13. BLOOMBECKER JJ.
      SHORT-CIRCUITING COMPUTER CRIME.
      DATAMATION, 1989 OCT 1, V35 N19:71-72.

14. FORCHT KA; THOMAS D; WIGGINTON K.
      COMPUTER CRIME - ASSESSING THE LAWYERS PERSPECTIVE.
      JOURNAL OF BUSINESS ETHICS, 1989 APR, V8 N4:243-251.



X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X
 Another file downloaded from:                                NIRVANAnet(tm)

 &TOTSE                510/935-5845   Walnut Creek, CA         Taipan Enigma
 Burn This Flag        408/363-9766       San Jose, CA                Zardoz
 realitycheck          415/666-0339  San Francisco, CA    Poindexter Fortran
 Governed Anarchy      510/226-6656        Fremont, CA             Eightball
 New Dork Sublime      805/823-1346      Tehachapi, CA               Biffnix
 Lies Unlimited        801/278-2699 Salt Lake City, UT            Mick Freen
 Atomic Books          410/669-4179      Baltimore, MD               Baywolf
 Sea of Noise          203/886-1441        Norwich, CT             Mr. Noise
 The Dojo              713/997-6351       Pearland, TX               Yojimbo
 Frayed Ends of Sanity 503/965-6747     Cloverdale, OR              Flatline
 The Ether Room        510/228-1146       Martinez, CA Tiny Little Super Guy
 Hacker Heaven         860/456-9266        Lebanon, CT         The Visionary
 The Shaven Yak        510/672-6570        Clayton, CA             Magic Man
 El Observador         408/372-9054        Salinas, CA         El Observador
 Cool Beans!           415/648-7865  San Francisco, CA        G.A. Ellsworth
 DUSK Til Dawn         604/746-5383   Cowichan Bay, BC         Cyber Trollis
 The Great Abyss       510/482-5813        Oakland, CA             Keymaster

                          "Raw Data for Raw Nerves"
X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X
