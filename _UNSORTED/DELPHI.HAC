
                    D E L P H I   H A C K I N G   F I L E

                                    V1.0


This is a file written for those of us who would like free online service
accounts, Internet access, and other services by using fraudulent accounts
or other illegal means of attaining them.

This file simply tells you the basics on doing this to the online service
DELPHI. With DELPHI, you get immediate access with valid information and
once you're on the system there's a variety of things you can do to get
the maximum use out of your account(s). That's what is in this file,
plain and simple. I focus mostly on the Conference Area, DELPHI's version
of chat-rooms. Enjoy!

This file was written by Jim Morrison in December 1995. Feel free to
distribute.

NOTE that this file is for people who are not accessing DELPHI through
the Beta Software.

CONTENTS
1 - Accounts
  1.1 - Getting ready to set up a fraudulent account
  1.2 - Setting up fraudulent accounts
  1.3 - Access methods
2 - Mail
  2.1 - Mailbombing
  2.2 - Set forward command
  2.3 - Fake forwards
  2.4 - Fakemail and port 25
  2.5 - Fakemail bombs
  2.6 - Getting people kicked off
  2.7 - Phishing
3 - Conference
  3.1 - Keysubs and macros
  3.2 - Intercepting pages
  3.3 - Text manipulation
  3.4 - Fake sends
  3.5 - Annoyance features
  3.6 - Scramble and free areas
  3.7 - Shutting down Conference
  3.8 - Special areas
4 - Miscellaneous
  4.1 - Multiple telnets
  4.2 - Setting up new accounts
  4.3 - Forums
5 - People
  5.1 - Employees
  5.2 - Hackers
  5.3 - Special thanks


                                1 - ACCOUNTS


Accounts are practically the only way to get on the system. If you aren't
planning on paying, and don't already have an account, then I suggest
creating a fraudulent one, unless you're a really good guesser of other
people's passwords :)

1.1  Getting ready to set up a fraudulent account

If you call directly from your modem line to the toll-free number, you're
going to get caught. If you use any of your own information during signup,
you're probably going to get caught. Just use common sense.
You can either set them up by AT&T or by a dialout (see below). The old
method of telnetting to delphi.com to set up an account no longer works. I
suggest using the Internet for now, because I'm still not totally sure you
can't get traced through AT&T.
You'll need the information that you're going to use for the account
ahead of time. If this is your first time, use a local name and address.
The name or address doesn't have to be real, just have a valid city,
state, zip, and phone number. The person can live anywhere in the US
or Canada, but I suggest just using US addresses for now.
Also have a valid credit card number ready to use. Sometimes DELPHI
alternates and won't take certain card types, but don't worry about it..
you don't always get an account set up on your first attempt.

1.2  Setting up fraudulent accounts

There's two ways to call their toll-free signup numbers:
1) AT&T - Dial on one phone 1-800-CALL-ATT. Have a valid credit card ready.
   Choose option 1, then type in either 800-695-4002 or 800-365-4636 when
   they ask for the phone number you'd like to call. Then they'll have you
   type in your credit card number and expiration date. Have the computer
   on the same phone line and have your modem pick up by typing ATDT in
   your communications software the second after you type in your credit
   card number. Hang up the phone you dialed from.. your computer will
   connect you.
2) DIALOUT - Telnet to a dialout (modem.d.umn.edu, for example) and type
   ATZ (if it's working, it'll respond with "OK"). Then type
   ATDT918006954002 or ATDT918003654636 (note that most dialouts use PBX's
   so you have to dial a 9 before the toll-free number). After a few
   seconds, you're in.
Once you get the word CONNECT after using either way to access the signup
lines, wait a few seconds, then press RETURN. It'll prompt you for a
temporary password. "FREE" will get you in every time, but you can also
use passwords like CUSTOM5, CUSTOM7, LAWFORUM, DIXIE, KMA987, IP22, DG87,
etc. It'll then ask for your full name, address, city, state, zip, and
phone number.
After that you get to choose a Username. Usernames can be 3-12 characters
(spaces don't count). I've noticed that anything with the word PUSSY,
FUCK, CUNT, BITCH, etc. no longer works, but you can still get away with
ASS if you're looking to offend people ;)
After choosing a name it'll ask for your billing method. Though it doesn't
list AMEX (American Express) as a valid option, it's still there to choose
from. Type in the card number and expiration date. Then you're prompted
for your mother's maiden name (just make up some surname) and your
temporary password.
Then it processes. If you get an AGREE prompt, you're in. If it wants to
find your local access number, disconnect and try again.. because they're
not going to give you an account for some reason.. chances are it has to
do with the credit card you gave them.
At the AGREE prompt, they ask you to type AGREE in, but you can type something
like AGREENOT!!, or AGRE, etc. and still take it, making it void if you ever
were caught.
Choose local access numbers accordingly. If you call a number other than the
one you specify to them, it's going to look suspicious and if they find out,
you're history.
At the end of the signup procedure, they give you access instructions.
Enjoy.. because now you have an account :)

1.3  Access methods

You can either use the AT&T method of calling long distance access numbers
(or you're own long distance since they can't really trace you from non-ANI
numbers). I'd suggest using *67 as a dialing prefix before calling a local
access number, however.
You can have more than one person on an account, though it usually gets
deleted faster when you do that or if you access from a different access
number than you specified to them. Using real information with a stolen
account helps the account last alot longer as well, compared to generated
and extrapolated credit cards.


                                  2 - MAIL


E-Mail can be quite a powerful tool when you're online. All mail on DELPHI
is sent through the BOS node, that's why you see BOS:: before a username
when that person sends you mail. You can also send faxes and postal mail,
but I wouldn't suggest saying anything personal, since DELPHI has been
known to read certain user's mail.

2.1  Mailbombing

The easiest way to send a mailbomb is to go into the conference area and
program this string into a macro or keysub (see 3.1):

/m USERNAME Message

Replace USERNAME with the person you want to bomb/spam, and Message
with whatever you'd like to tell them. Each time you do this, it sends
one piece of mail to their E-Mail box. The Message line is the subject
line of the message.
You can mailbomb several people at once by typing commas in between
usernames like this:

/m ADAB,CHORH you're both fucking whores.

Don't ever mailbomb from your account if it is a real account, because
your username shows up on every message they get. Mailbombing will get
you suspended from DELPHI, usually about 24 hours after the offense.

2.2  Set forward command

If you are the victim of a mailbomb, there are a few interesting tools
you can use to fight back. If the mailbomb is still taking place while
you're in MAIL (You can enter MAIL in Conference by typing /MAIL),
simply type SET FORWARD at the MAIL> prompt and type in the Username
of the person bombing you (this will forward all your mail to his/her
account, so when he/she tries to send you mail, they'll get it right back
at them) or you can forward all your mail to SERVICE or ROOT or any other
administrative account that you know of. This will get them suspended,
but it's not suggested if you're doing it from your real account. Just
set the forward back to them in that case and forward one mail to SERVICE
telling them you've been mailbombed.
NEVER mail SERVICE, even if you are reporting someone, from your fraudulent
account however, because they will delete you AND the other person then.
You can delete all your messages with ease by typing DEL /ALL at the

MAIL> prompt.

Don't forget to set your forward back to normal by typing SET NOFORWARD.
If you want it to look like you're dead, account is deleted, etc. you
can type ANY word in place of the username you want your mail forwarded
to, so if you set it to DELETED or DEAD, when a person tries to mail you
they get:

No such user DELETED

If you think your account is going to be deleted for real soon, you can
set the forward to another one of your usernames (though DELPHI can see
that you've set your forward and will probably look into all the names
it might be set to) or an Internet address. Once again, don't set your
forward to your own account or real Internet address from a fraudulent
account.

2.3  Fake forwards

If you want to forward a piece of false mail to some user that supposedly
was sent to you by another user, there's a way. It can also be used
to trick SERVICE into thinking somebody mailed you and left you a
death threat, but it's not suggested from your real account nor from
a fake one anymore.
Here's the format for fake forwards. Actual number of spaces shown:

From:   BOS::USERNAME        1-DEC-1995 00:00:00.00
To:     YOUR USERNAME
CC:
Subj:   Test

Test Message Here.

Be sure to use a correct date format, your own username in the To:
prompt, and use the right number of spaces, or it's going to look
suspicious. To send one, just type SEND at the MAIL> prompt (not
FORWARD, because it's not a real forward), type a subject line,
and then type in the above format your forwarded message. Don't include
anything else in the body of your message because it is not possible
to add anything in, except for the subject line.

2.4  Fakemail and port 25

It is possible to send fake Internet mail (it will look like an Internet
message, not a DELPHI message) to anyone FROM anyone on any system.
To do this, find an Internet provider that lets you telnet to Port 25
of certain telnet addresses (DELPHI is not one of them).
Then telnet to DELPHI.COM,25 or NAUTICOM.NET,25, etc. and you should get
a message informing you of the that you have connected and that a certain
language is spoken there. Just ignore this and type HELO delphi.com.
At this point type MAIL FROM: and the username of the person you want the
mail to be from. If you're not connected to Port 25 of DELPHI, then add
a @delphi.com to the end of the username, otherwise the username will
appear to have come from the system you're currently connected to.
Then type RCPT TO: and the username of the person you want the fakemail
to go to. While you could send from any name, even non-existant, you
can only send to an actual username. Once again, if you're on any other
mail port besides DELPHI.COM, then add a @delphi.com qualifier to the
end of their username or the message will never make it to DELPHI.
Once you get OK messages for both of the above, type DATA, press
and it will tell you to go ahead and type your message below. Press
RETURN once at this point so there's a space between the header files
of the message and your fake message.
Once you're done typing, end it with a "." (without the quotes) on a
separate, blank line. Once it says that the message has been received
for delivery, it simply means that that message has been sent.
You can type as many messages as you want to any number of people.
Once you are finished, type QUIT and it will close the connection.

2.5  Fakemail bombs

You can send a person a fakemail bomb from another person by following
the steps above but repeatedly sending the same message over and over
again. I suggest doing this by macros. Keysubs don't work in any place
other than the Conference Area.

2.6  Getting people kicked off

If you don't like a particular user on the system, there's a variety
of ways of getting him kicked off:
1) Manipulate what he says in the Conference Area in hopes that others
   will report him (see 3.3 and 3.4).
2) Send a fake forward to SERVICE allegedly from that user to you
   threatening your life or something.
3) Send fakemail from that user to any other user threatening their
   lives or anything else of an offensive nature.
4) Send fakemail from that user to SERVICE telling them you'd like to
   cancel your account.
5) Send a fakemail bomb from that user to any other user, including
   SERVICE. Despite what they say, ANYONE can send Internet E-Mail
   without having to register for Internet Services by typing the format
   IN%"address@goes.here.com" at the To: prompt.
6) Breaking onto a real account and reporting the user.
If none of these methods work the first time, try another. Don't forget that
many small complaints can add up to a big one get a user kicked off for good.

2.7  Phishing

There are ways that you can scam users for their password with the help
of fakemail and an extra Internet account (that's not registered under
your name). It can involve sending email from SERVICE to a number of
users telling them to change their password to an assigned password
because of maintenance problems, etc., or tell them to mail their
password to another Internet address because they won free time, a
local server went down, etc. Be creative, but remember that you can't
trick all the people all the time.
Trust me, I haven't yet.


                              3 - CONFERENCE


Conference is the life-line of DELPHI. It can either be used to learn about
other people or torment them.
The actual Conference program is called PBATCH, and operates in a hidden
group 0 named " Private Node CO (sysops welcome)." Note that you cannot
name a group name starting with a space, but this group is named in that
fashion. In it resides several nodes operating under the PBATCH Program/
Username with nicknames like *WHO_BOS1A, etc. The group looks like this:

0)   Private Node CO (Sysops welcome)
      *WHO_BOS1E, *WHO_BOS1A, *WHO_BOS1B, *WHO_BOS1C, *WHO_BOS1D, *WHO_BOS1F,
  *WHO_BOS1G

While it is virtually impossible to get inside the group since it is
private, if you were inside it what you'd see would be Login and Logoff
messages being displayed by the several nodes, along with probably New
Mail messages, etc. or anything else to do with specific nodes.
If you would like more information on this, E-Mail PETER, who is the
Conference programmer for DELPHI.
We will deal with a few Conference tricks now in case you end up being the
person tormented, or just want to go out for vengeance upon other users.


3.1  Keysubs and macros

This is a brief summary of how you can use keysubs and macros in Conference.
Please note that keysubs are part of DELPHI, and can only be used in
Conference. Macros stay in your software and are there permanantly or until
you remove them.
The usage parameters for /KEYSUB is as follows:

/KEYSUB $IDENTIFIER$COMMANDS/COMMENTS $

For example:

/KEYSUB $$Fuck $ would echo Fuck everytime you press RETURN.
/KEYSUB $,$Fuck $ would echo Fuck evertime you typed a "," and RETURN.

To turn /KEYSUB off, type /NOKEYSUB. There will be more discussed on
Keysubs later in 3.5
Macros will depend on your specific communications software, but they
are generally more effective than Keysubs because everytime you press
the key or key combination on your keyboard, it produces the same
result, as programmed in by you.
For example, if you programmed the word Fuck as F1, everytime you would
press the F1 key, the word Fuck would appear. It will appear without
pressing RETURN unless you add a ^M after it. ^I is a tab and ^G is a
beep.
Both of these will come in handy, as you will later see in this section.

3.2  Intercepting pages

If a user has been paged but is no longer in the Conference Area, you can
type /NAME and the name of the person getting paged.
You can then answer that page by typing /ANSWER or /ACCEPT. I haven't tried
this before, but I assume it works.

3.3  Text manipulation

You can manipulate what someone says in a chat room or produce a fake
message and get away with it IF the people you want to trick have their
widths set at 80 and have /SHOWRN off. You can still try this if your
width isn't at 80, even though it won't look normal on your computer,
but to others who have their widths at 80 (the default width) there
won't be a difference.
The length of your Username/nickname and what you type DOES make a
difference, but one proven method is to:
1) Change your nickname to a one-character name.
2) Type an 8 character string, such as "whatsup?" or "hmmmmmmm"
3) Tab 9 times or program ^I into a macro 9 times without pressing RETURN
   or using a ^M in the macro.
4) Type the person's name and what you want them to say. Remember to add
   in a period before their nickname and to capitalize their username if
   they aren't using a nickname. You can also type other messages (see
   below).
Some other messages instead of manipulating what somebody says might be:

New mail received on node BOS1C from BOS::USERNAME

or

Conference will be shutting down in 5 minutes.

You may add a beep before your message by pressing CTRL-G and RETURN.
In a macro, you'd type ^G, the message, and then ^M, so the macro would now
look something like this:

^I^I^I^I^I^I^I^I^I^GNew mail received on node BOS1C from BOS::SERVICE^M

Which would look something like this in the chat room if your nickname was
"S" and you typed "Whastup?":

.S> Whatsup?

New mail received on node BOS1C from BOS::SERVICE

If your not sure of how it's going to turn out, just go into a private
group by yourself and start experimenting. It's all in the Tabs.

3.4  Fake sends

You can also send fake /SENDS, one of two ways:
1) Simply use the method above, but substitute a CTRL-G and the SEND
   where you would normally type any other message.
2) Type the fake send when you are /SENDING that individual, that way only
   they will see it, as shown below.
Type /SEND and their name, followed by an 8 character string, and then tab
18 times, press CTRL-G, type out the send, and tab an additional 10 times
to add an extra space between the send and the chat room conversation.
This way it will look like it's double-spaced, which is how DELPHI spaces
each SEND you receive.
A macro would look like this:

^I^I^I^I^I^I^I^I^I^I^I^I^I^I^I^I^I^I^GSERVICE>> Fuck you^I^I^I^I^I^I^I^I^I^I^M

And would appear like this to the other individual:

YOURUSERNAME>> Whatsup?

SERVICE>> Fuck you

You can always experiment by /SENDING yourself first. It may sound
complicated at first, but it's a proven method of tricking people.

3.5  Annoyance features

There are many ways to annoy people on DELPHI. A few are:
 1) CTRL-G to cause beeps
 2) Mailbombs/Spamming
 3) Text manipulation
 4) Fake sends
 5) Fake signing off by typing "- signed off -"
 6) Entering a chat room that has two people in it, fake sign off, then
    sit there quietly and listen in on their conversation, and start
    fake sending them from EACH OTHER just to piss them off and watch
    the confusion set in.
 7) Fake forwards
 8) Fakemail and fakemail bombs
 9) Changing your nickname to look similar to their Username. If they have
    an I in their name, replace it with a lowercase L in yours, or vice
    versa.
10) Mass Paging (can be used with both keysubs and macros)
11) Mass Sending (can be used with both keysubs and macros)
12) Paging every user on the system at once, causing an intolerable Idle
    list for many users, since it scrolls several pages down the screen
12) /Sending them while squelching them (with the /SQUELCH command) so they
    can't reply.
I'm sure there are hundreds of others out there as well.. these are the only
that come to mind right now. Forgive me if I left something out :)
Here are some examples of how you would use keysubs to perpetrate the above
instances:

/KEYSUB $$/P USERNAME $ - To page a user each time you press RETURN.

/KEYSUB $$/S or /Z USERNAME Message $ - Can be used for mass sending/zending.

/KEYSUB $$/M USERNAME Message - For mailbombing

/KEYSUB $$/S USERNAME ^G^G^G^G^G^G^G^G^G^G^G^G^G^G^G^G^G^G^G^G^G^G$ - Will
sometimes effectivly log them off if done in excess as long as they have
beeps turned on and the /BUSY flag isn't set.

3.6  Scramble and free areas

To win at Scramble Word Games more often on DELPHI, you may want to create
a word list in your DOS Edit program and upload it everytime you're playing
a Scramble Game. This will allow you to scroll words down screen and attain
higher scores.
Through this technique, I am the second highest scorer in Scramble history
with 2677 as a score under the username LAMENT.
In a few instances GAMEs may disappear from the Conference area. If this
occurs, simply nickname yourself GAME and create a group called Scramble
Word Game, so once they come back they will be forced to create a different
group name since the traditional one, Scramble Word Game, is already in
use. The pleasure in doing this is minimum, however, as sometimes you may
have to wait hours for the GAMEs to be reset. However, it can be fun if
somebody enters the group and thinks you really are GAME.
There are also Scramble Games in the Free Area under the Using DELPHI menu.
Type GO USING NEW CON at the Main Menu to access it. Note that it's only
open on certain evenings.

3.7  Shutting down Conference

It has been done before, though not by me. It's not too hard to do, either.
Just open multiple telnet sessions (see 4.1), upload a large text file
and /SEND it to yourself by using the keysubs, then squelch yourself right
in the middle of it. It will cause an overflow of data, forcing Conference
to shut down temporarily.
If you want to hear from the expert on this get in touch with me.

3.8  Special areas

As noted in the Introduction to this section there is a group 0 and other
nodes located in Conference, but only hidden. Sometimes, with the help of
system glitches and what not, you are thrown into these areas (while not
actually inside group 0) and see weird occurences. These have mainly
become a thing of the past but if you are ever thrown into these areas
this is what you may experience:

- All the GAMEs have disappeared
- There are a considerably less number of users in Conference than there was
  a second ago
- When you list the Available list, you see Employee accounts and sub-programs
  like NI-CONFAGENT which aren't even usernames.
- Group 0 with DELPHI's nodes operating under the username PBATCH (a program
  much like NI-CONFAGENT)
- JOINDELPHIs (People signing up for an account in North America) and
  GODELPHIs (People signing up for an account under UKDELPHI) in the Available
  List
- Unable to /SEND or /ZEND certain people that are on the system.
- SERVICE logged on occasionally in the Avaiable List

A neat trick is to /ZEND all the JOINDELPHIs and GODELPHIs telling them that
they're being traced and to log off immediately, or that DELPHI is nothing
but a shit service (very true) and that it just ain't worth their time to
sign up. You'll see that over half of them log off by the next time you
/ZEND them :)
Not much else to do here. You can /ZEND SERVICE, etc., but that's not always
wise. To get back into regular conference, simply exit and then come back.
You are usually thrown into areas though glitches that you don't even
notice until you see half the people in Conference are no longer there.


                             4 - MISCELLANEOUS


In this section are just some miscellaneous notes that couldn't really be
fitted in elsewhere. These includes multiple telnet sessions, setting up
more accounts from one that already exists, and an introduction to forums
and Moderator commands.

4.1  Multiple telnets

If you are telnetting to DELPHI on a provider that allows multiple telnet
sessions to be opened up at one time, then you can enter DELPHI several
times on the same or different accounts, which comes in handy for things
like shutting down Conference (see 3.6) or just for annoyance purposes.
You may also telnet into DELPHI from your own account on DELPHI by going
to the Internet SIG, registering for Internet use, then typing DELPHI.COM
in where you want to telnet to. You will only be able to control the
last session you telnetted from, and will either have to hang up (not
recommended) or individually sign off each account to get back to your
main connection.
This is troublesome to DELPHI because it causes system lag and can
ultimately lead to many unfortunate mishaps :)

4.2  Setting up new accounts

You can telnet to a dialout once on your fraudulent account (not suggested
if you own a real account) and set up additional accounts without having
to even leave the system. If they are watching the account you telnet
from to set up the new accounts, however, they will know about all the
accounts you set up and any other activity on your account.

4.3  Forums

Many Custom Forums exist throughout DELPHI with a wide range of topics
that many members may be interested in. Each of these forums have
Conference areas specifically designated for that area. The areas are
similar to the Main Conference Area.
You can check to see a user's real name or when a particular user was
last on by typing this format:

/ENTRY USERNAME

Also, there are many secret commands used by Moderators, Hosts, etc.
in these smaller Conference areas:

/XMOD          Turns the group into a moderated group or switches
/NOXMOD        it back to normal.  Only works for people with
               W, A, or C flags. W and A flags can transfer the
               moderator status to another user by typing
               /XMOD name. Moderator is given usage of all group
               functions regardless of who started the group.

/XPUB          Publishes the groupname in DELPHI MAIN.  Use
/NOXPUB        sparingly, when there is a good reason to make
               the presence of your CO known to people outside
               the SIGs.

/XSPON sched   Cause billing to be shut off for people who are
/NOXSPON       in the conference group.  Requires approval by
               DELPHI for a specific date, time, and duration.

/XGLISTEN      Make the group an Audience group.  Anyone joining
/NOXGLI        the group from that point on will be in listen-only
               mode and will not be able to say anything to the
               rest of the group unless the moderator recognizes
               him with a /NXL.  /GN shows who is talking in an
               Audience group. Anyone who is listen-only and who
               tries to talk will be instructed to use the /QUEUE
               command.

/XLISTEN name  Keep a person from talking in a group, whether it
/NOXL name     is an Audience group or not.  /NOXLISTEN allows a
               person to talk.  The person whose name you use in
               these commands gets told that he is "on stage" or
               "listening in the audience" when the commands are
               issued.

/NXL           (This is the same as /NOXLISTEN).  This is used by the
               moderator to recognize a speaker.  It starts the queued
               message.  After the message is done, you can use /XL
               to put the speaker back in the audience, but you don't
               have to (i.e., a guest can queue a message and remain
               in talk mode after).


                                 5 - PEOPLE


In this section I included some information about several employees,
so-called hackers, and finally my special thanks.

5.1  Employees

Some employees include:
DRSAX - Laurence Sax? Security employee
HAUNTEDHOUSE/CRYAN - Chris Ryan, Security employee
ICEBLINK - Security employee?
SECURITY - The main security account, operated by a number of individuals
SERVICE - Operated by Richard Learoyd (DELPHI Online Support Specialist),
          Tim Wingo (usually security matters), and Kevin Plankey (Domestic
          affairs)
RICHARD - Richard Learoyd
KEVIN/HACKTRACK - Kevin Michael Plankey, Member Servies Manager
CHAIKIN - John Chaikin, old Head of Security
CARYW - Cary Watson, Security employee
JSRIBERG/PHISH - Jonathon Sriberg
SOCRATES - Hosts the New Member Conferences in the free area.
ASKDELPHI - Information about DELPHI, operated by a number of individuals
CHARLEEN - Charleen, sometimes operates the SERVICE account in place of
           Richard Learoyd
KIP/GAME - Kip, operates GAMEs and uses his account to operate certain Games.
RMESARD/GROOVASAURUS/RONM - Ron Mesard, DELPHI employee
MZANGER - Mark Zanger, DELPHI Senior Editor

Other alleged employees include:

NFISH, ANDERSON1160, LUEDTKE, KHAMADA, PIEDMONT, SKLEIN, BTREMP, GOOMP,
WAYNE46, CHETAY, SHEREE, CLYDEFLOWERS, EBRESNER, LEIGHANN, SUGLIO, JBUCHER,
OSBAN, TBROOKS22, WHITEW, CMUISE, COATES132, DECARR, AMYC, BADUBLINSKE,
RLPARSONS, ELI27, DGMANNEL, JKCRAUN, DGEN, OWENTIME, DANIELCROSBY,
DAVIDNASON, CASE, DEAN144, RWBROOKS, JERRYAUTHOR, SS_RAT, CCORN, LPRIBANIC,
JHTAY, ABLEE, VIPERDOG, BENJMN, MOHATMA, MAZMAN, PJORSTAD, RSALIK, CHUCKT,
IANJ, KFRERKER, MAZOUE, KIRKPATRICK, SKYCLAD, DARREN42, KATHYCOBB, MADBRO,
FRANKS, DBOX, CRUISE, RGERBER, JHERBST, WCIESLA, GARY1324, FREDL, SHAWN89,
NAZIMEK34, MICHAELP, STESMI, MAVRACHANG, RODBER, NILSON, MSTEEVES, KINGUANA,
PEAPI, JAMIE_K, CAROLAK, LISA, YAKFNORD, NI-CONFAGENT, DTEN, JULIE, ECAREY,
RGS, MARK2122, JPOLITO, DARREN, TJMARTINEZ, DAVECLEMENTS, RANDYALLMAN,
RCJOHNSON, JOHNPROBERTS, 4990TCW, HANKEL, BRUMIT, CRUISE, SHANNONLA, KRAFTWIZ,
ROCKETMAN666, DENLEWIS, GARYREYNOLDS, DAVEW, LUCAS330, CH567, ARTSIMON,
PHASSETT, JOEBRADY, HEIDI, SARAH, FRANK, JIM, RJOHNSON, and any other
suspicious looking name would qualify. Usually on during regular business
hours as well.

5.2  Hackers

Many "hackers" frequent DELPHI. I use the term hackers loosely due to the
fact that "hacking" DELPHI isn't as much work as hacking into bank accounts
or the Pentagon. But, since it's still hacking in my book, and most everyone
else's, I'll use that term for people who use fraudulent accounts or phish
passwords/credit cards off other people.
I won't give any usernames or aliases of such people but most anyone who
"hacks" into DELPHI by way of fraudulent accounts would qualify as such.

5.3  Special thanks

Special thanks to all those that helped me along my way here, and taught me
the ways of DELPHI. Many have left, but there are a few that stand out in
my mind right now: Blah, Adam, Tisdal, Henley52, Neil, PiL.

And to all my helping hands: Eddie, June, Anj, sillygirly, Michele, and
anyone else I may have failed to mention in this version.


THE END
