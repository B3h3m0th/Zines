Chaos Digest               Lundi 12 Avril 1993          Volume 1 : Numero 18

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.18 (12 Avril 1993)
File 1--Cryptographie a clef publique: PGP

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost by sending a message to:
                linux-activists-request@niksula.hut.fi
with a mail header or first line containing the following informations:
                    X-Mn-Admin: join CHAOS_DIGEST

The editors may be contacted by voice (+33 1 47874083), fax (+33 1 47877070)
or S-mail at: Jean-Bernard Condat, Chaos Computer Club France [CCCF], B.P.
155, 93404 St-Ouen Cedex, France.

Issues of ChaosD can also be found on some French BBS.  Back issues of
ChaosD can be found on the Internet as part of the Computer underground
Digest archives.  They're accessible using anonymous FTP from:

        * kragar.eff.org [192.88.144.4] in /pub/cud/chaos
        * uglymouse.css.itd.umich.edu [141.211.182.91] in /pub/CuD/chaos
        * halcyon.com [192.135.191.2] in /pub/mirror/cud/chaos
        * ftp.cic.net [192.131.22.2] in /e-serials/alphabetic/c/chaos-digest
        * ftp.ee.mu.oz.au [128.250.77.2] in /pub/text/CuD/chaos
        * nic.funet.fi [128.214.6.100] in /pub/doc/cud/chaos
        * orchid.csv.warwick.ac.uk [137.205.192.5] in /pub/cud/chaos

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Wed Mar 24 15:03:59 CDT 1993
From: celma_s@epita.fr (Samuel Celma )
Subject: File 1--Cryptographie a clef publique: PGP


                 Phil Zimmerman's PGP (Pretty Good Privacy)
                    Public Key Encryption for the Masses


[ PGP Documentation extracts ]

What is PGP?

Pretty Good(tm) Privacy (PGP), from Phil's Pretty Good Software, is a
high security cryptographic software application for MSDOS, Unix,
VAX/VMS, and other computers.  PGP allows people to exchange files or
messages with privacy, authentication, and convenience.  Privacy
means that only those intended to receive a message can read it.
Authentication means that messages that appear to be from a
particular person can only have originated from that person.
Convenience means that privacy and authentication are provided
without the hassles of managing keys associated with conventional
cryptographic software.  No secure channels are needed to exchange
keys between users, which makes PGP much easier to use.  This is
because PGP is based on a powerful new technology called "public key"
cryptography.

PGP combines the convenience of the Rivest-Shamir-Adleman (RSA)
public key cryptosystem with the speed of conventional cryptography,
message digests for digital signatures, data compression before
encryption, good ergonomic design, and sophisticated key management.
And PGP performs the public-key functions faster than most other
software implementations.  PGP is public key cryptography for the
masses.

PGP does not provide any built-in modem communications capability.
You must use a separate software product for that.

Why Do You Need PGP?

It's personal.  It's private.  And it's no one's business but yours.
You may be planning a political campaign, discussing your taxes, or
having an illicit affair.  Or you may be doing something that you
feel shouldn't be illegal, but is.  Whatever it is, you don't want
your private electronic mail (E-mail) or confidential documents read
by anyone else.  There's nothing wrong with asserting your privacy.
Privacy is as apple-pie as the Constitution.

Perhaps you think your E-mail is legitimate enough that encryption is
unwarranted.  If you really are a law-abiding citizen with nothing to
hide, then why don't you always send your paper mail on postcards?
Why not submit to drug testing on demand?  Why require a warrant for
police searches of your house?  Are you trying to hide something?
You must be a subversive or a drug dealer if you hide your mail
inside envelopes.  Or maybe a paranoid nut.  Do law-abiding citizens
have any need to encrypt their E-mail?

What if everyone believed that law-abiding citizens should use
postcards for their mail?  If some brave soul tried to assert his
privacy by using an envelope for his mail, it would draw suspicion.
Perhaps the authorities would open his mail to see what he's hiding.
Fortunately, we don't live in that kind of world, because everyone
protects most of their mail with envelopes.  So no one draws suspicion
by asserting their privacy with an envelope.  There's safety in
numbers.  Analogously, it would be nice if everyone routinely used
encryption for all their E-mail, innocent or not, so that no one drew
suspicion by asserting their E-mail privacy with encryption.  Think
of it as a form of solidarity.

Today, if the Government wants to violate the privacy of ordinary
citizens, it has to expend a certain amount of expense and labor to
intercept and steam open and read paper mail, and listen to and
possibly transcribe spoken telephone conversation.  This kind of
labor-intensive monitoring is not practical on a large scale.  This
is only done in important cases when it seems worthwhile.

More and more of our private communications are being routed through
electronic channels.  Electronic mail will gradually replace
conventional paper mail.  E-mail messages are just too easy to
intercept and scan for interesting keywords.  This can be done
easily, routinely, automatically, and undetectably on a grand scale.
International cablegrams are already scanned this way on a large
scale by the NSA.

We are moving toward a future when the nation will be crisscrossed
with high capacity fiber optic data networks linking together all our
increasingly ubiquitous personal computers.  E-mail will be the norm
for everyone, not the novelty it is today.  Perhaps the Government
will protect our E-mail with Government-designed encryption
protocols.  Probably most people will trust that.  But perhaps some
people will prefer their own protective measures.

Senate Bill 266, a 1991 omnibus anti-crime bill, had an unsettling
measure buried in it.  If this non binding resolution had become real
law, it would have forced manufacturers of secure communications
equipment to insert special "trap doors" in their products, so that
the Government can read anyone's encrypted messages.  It reads:  "It
is the sense of Congress that providers of electronic communications
services and manufacturers of electronic communications service
equipment shall insure that communications systems permit the
Government to obtain the plain text contents of voice, data, and
other communications when appropriately authorized by law."  This
measure was defeated after rigorous protest from civil libertarians
and industry groups.  But the Government has since introduced other
disturbing legislation to work toward similar objectives.

If privacy is outlawed, only outlaws will have privacy.  Intelligence
agencies have access to good cryptographic technology.  So do the big
arms and drug traffickers.  So do defense contractors, oil companies,
and other corporate giants.  But ordinary people and grassroots
political organizations mostly have not had access to affordable
"military grade" public-key cryptographic technology.  Until now.

PGP empowers people to take their privacy into their own hands.
There's a growing social need for it.  That's why I wrote it.

Legal Issues

Trademarks, Copyrights, and Warranties
--------------------------------------

"Pretty Good Privacy", "Phil's Pretty Good Software", and the "Pretty
Good" label for computer software and hardware products are all
trademarks of Philip Zimmermann and Phil's Pretty Good Software.  PGP
is (c) Copyright Philip R. Zimmermann, 1990-1993.  Philip Zimmermann
also holds the copyright for the PGP User's Manual, as well as any
foreign language translations of the manual or the software.

The author assumes no liability for damages resulting from the use of
this software, even if the damage results from defects in this
software, and makes no representations concerning the merchantability
of this software or its suitability for any specific purpose.  It is
provided "as is" without express or implied warranty of any kind.


Patent Rights on the Algorithms
-------------------------------

When I first released PGP, I half-expected to encounter some form of
legal harassment from the Government.  Indeed, there has been legal
harassment, but it hasn't come from the Government-- it has come
from a private corporation.

The RSA public key cryptosystem was developed at MIT with Federal
funding from grants from the National Science Foundation and the
Navy.  It is patented by MIT (U.S. patent #4,405,829, issued 20 Sep
1983).  A company in California called Public Key Partners (PKP) holds
the exclusive commercial license to sell and sub-license the RSA
public key cryptosystem:


+++++
XRPX Acc No: N83-178106
    Cryptographic communication system has encoding and decoding devices
    coupled to memory so that data words may be stored ensuring file
    integrity
Patent Assignee: (MASI ) MASSACHUSETTS INST TECH
Author (Inventor): RIVEST R L; SHAMIR A; ADLEMAN L M
Number of Patents: 001
Patent Family:
    CC Number    Kind     Date      Week
    US 4405829     A     830920     8340    (Basic)
Priority Data (CC No Date): US 860586 (771214)
Abstract (Basic): The system includes a  communications channel coupled to
    at least one terminal having an encoding device and to at least one
    terminal having a decoding device.  A  message-to-be-transferred is
    enciphered to ciphertext at the encoding terminal by encoding the
    message as a number M in a predetermined set.  That  number is then
    raised to a first predetermined power (associated with the intended
    receiver) and finally computed.  The remainder or residue, C, is
    computed when the exponentiated number is divided by the product of two
    predetermined prime numbers (associated with the intended  receiver).
         The residue C is the ciphertext.  The ciphertext is decipheres to
    the original message at the decoding terminal in a similar manner by
    raising the ciphertext to a second predetermined power (associated with
    the intended receiver).  The residue, M', is computed when the
    exponentiated ciphertext is divided by the product of the two
    predetermined prime numbers associated with the intended receiver.  The
    residue M' corresponds to the original encoded message M.   (12pp.)
Int Pat Class: H04K-001/00; H04L-009/04
+++++


The author of this software implementation of the RSA algorithm is
providing this implementation for educational use only.  Licensing this
algorithm from PKP is the responsibility of you, the user, not Philip
Zimmermann, the author of this software implementation.  The author
assumes no liability for any patent infringement that may result from
the unlicensed use by the user of the underlying RSA algorithm used in
this software.

Unfortunately, PKP is not offering any licensing of their RSA patent
to end users of PGP.  This essentially makes PGP contraband in the
USA.  Jim Bidzos, president of PKP, threatened to take legal action
against me unless I stop distributing PGP, until they can devise a
licensing scheme for it.  I agreed to this, since PGP is already in
wide circulation and waiting a while for a licensing arrangement from
PKP seemed reasonable.  Mr. Bidzos assured me (he even used the word
"promise") several times since the initial 5 June 91 release of PGP
that they were working on a licensing scheme for PGP.  Apparently, my
release of PGP helped provide the impetus for them to offer some sort
of a freeware-style license for noncommercial use of the RSA
algorithm.  However, in December 1991 Mr. Bidzos said he had no plans
to ever license the RSA algorithm to PGP users, and denied ever
implying that he would.  Meanwhile, I have continued to refrain from
distributing PGP, although I continue to update the PGP User's Guide,
and have provided the design guidance for new revisions of PGP.
Ironically, all this legal controversy from PKP has imparted a
forbidden flavor to PGP that has only served to amplify its universal
popularity.

I wrote my PGP software from scratch, with my own implementation of
the RSA algorithm.  I didn't steal any software from PKP.  Before
publishing PGP, I got a formal written legal opinion from a patent
attorney with extensive experience in software patents.  I'm
convinced that publishing PGP the way I did does not violate patent
law.  However, it is a well known axiom in the US legal system that
regardless of the law, he with the most money and lawyers prevails,
if not by actually winning then by crushing the little guy with legal
expenses.

Not only did PKP acquire the exclusive patent rights for the RSA
cryptosystem, which was developed with your tax dollars, but they
also somehow acquired the exclusive rights to three other patents
covering rival public key schemes invented by others, also developed
with your tax dollars.  This essentially gives one company a legal
lock in the USA on nearly all practical public key cryptosystems.
They even appear to be claiming patent rights on the very concept of
public key cryptography, regardless of what clever new original
algorithms are independently invented by others.  And you thought
patent law was designed to encourage innovation!  PKP does not
actually develop any software-- they don't even have an engineering
department-- they are essentially a litigation company.

Public key cryptography is destined to become a crucial technology in
the protection of our civil liberties and privacy in our increasingly
connected society.  Why should the Government try to limit access to
this key technology, when a single monopoly can do it for them?

It appears certain that there will be future releases of PGP,
regardless of the outcome of licensing problems with Public Key
Partners.  If PKP does not license PGP, then future releases of PGP
might not come from me.  There are countless fans of PGP outside the
US, and many of them are software engineers who want to improve PGP
and promote it, regardless of what I do.  The second release of PGP
was a joint effort of an international team of software engineers,
implementing enhancements to the original PGP with design guidance
from me.  It was released by Branko Lankester in The Netherlands and
Peter Gutmann in New Zealand, out of reach of US patent law.
Although released only in Europe and New Zealand, it spontaneously
spread to the USA without help from me or the PGP development team.

The IDEA(tm) conventional block cipher used by PGP is covered by a
patent in Europe, held by ETH and a Swiss company called Ascom-Tech
AG.  The patent number is PCT/CH91/00117.  International patents are
pending.  IDEA(tm) is a trademark of Ascom-Tech AG.  There is no
license fee required for noncommercial use of IDEA.  Ascom Tech AG
has granted permission for PGP to use the IDEA cipher, and places no
restrictions on using PGP for any purpose, including commercial use.
You may not extract the IDEA cipher from PGP and put it in another
commercial product without a license.  Commercial users of IDEA may
obtain licensing details from Dieter Profos, Ascom Tech AG, Solothurn
Lab, Postfach 151, 4502 Solothurn, Switzerland, Tel +41 65 242885,
Fax +41 65 235761.

The ZIP compression routines in PGP come from freeware source code,
with the author's permission.  I'm not aware of any patents on the
ZIP algorithm, but you're welcome to check into that question
yourself.  If there are any obscure patent claims that apply to ZIP,
then sorry, you'll have to take care of the patent licensing, not me.

All this patent stuff reminds me of a Peanuts cartoon I saw in the
newspaper where Lucy showed Charlie Brown a fallen autumn leaf and
said "This is the first leaf to fall this year."  Charlie Brown said,
"How do you know that?  Leaves have been falling for weeks."  Lucy
replied, "I had this one notarized."

Licensing and Distribution
--------------------------

In the USA PKP controls, through US patent law, the licensing of the
RSA algorithm.  But I have no objection to anyone freely using or
distributing my PGP software, without payment of fees to me.  You must
keep the copyright notices on PGP and keep this documentation with
it.  However, if you live in the USA, this may not satisfy any legal
obligations you may have to PKP for using the RSA algorithm as
mentioned above.

In fact, if you live in the USA, and you are not a Federal agency,
you shouldn't actually run PGP on your computer, because Public Key
Partners wants to forbid you from running my software.  PGP is
contraband.

PGP is not shareware, it's freeware.  Forbidden freeware.  Published
as a community service.  If I sold PGP for money, then I would get
sued by PKP for using their RSA algorithm.  More importantly, giving
PGP away for free will encourage far more people to use it, which
hopefully will have a greater social impact.  This could lead to
widespread awareness and use of the RSA public key cryptosystem,
which will probably make more money for PKP in the long run.  If only
they could see that.

All the source code for PGP is available for free under the "Copyleft"
General Public License from the Free Software Foundation (FSF).  A
copy of the FSF General Public License is included in the source
release package of PGP.

The PGP version 2.2 executable object release package for MSDOS
contains the PGP executable software, documentation, sample key rings
including my own public key, and signatures for the software and this
manual, all in one PKZIP compressed file called PGP22.ZIP.  The PGP
source release package for MSDOS contains all the C source files in
one PKZIP compressed file called PGP22SRC.ZIP.

You may obtain free copies or updates to PGP from thousands of BBS's
worldwide or from other public sources such as Internet FTP sites.
Don't ask me for a copy directly from me, since I'd rather avoid
further legal problems with PKP at this time.  I might be able to
tell you where you can get it, however.

If you have access to the Internet, watch for announcements of new
releases of PGP on the Internet newsgroups "sci.crypt" and PGP's own
newsgroup, "alt.security.pgp".  There is also an interest group
mailing list called info-pgp, which is intended for users without
access to the "alt.security.pgp" newsgroup.  Info-pgp is moderated by
Hugh Miller, and you may subscribe to it by writing him a letter at
info-pgp-request@lucpul.it.luc.edu.  Include your name and Internet
address.  If you want to know where to get PGP, Hugh can send you a
list of Internet FTP sites and BBS phone numbers.  Hugh may also be
reached at hmiller@lucpul.it.luc.edu.

Export Controls
---------------

The Government has made it illegal in many cases to export good
cryptographic technology, and that may include PGP.  They regard this
kind of software as munitions.  This is determined by volatile State
Department policies, not fixed laws.  I will not export this software
out of the US or Canada in cases when it is illegal to do so under US
State Department policies, and I assume no responsibility for other
people exporting it on their own.

If you live outside the US or Canada, I advise you not to violate US
State Department policies by getting PGP from a US source.  Since
thousands of domestic users got it after its initial publication, it
somehow leaked out of the US and spread itself widely abroad, like
dandelion seeds blowing in the wind.  If PGP has already found its
way into your country, then I don't think you're violating US export
law if you pick it up from a source outside of the US.

It seems to some legal observers I've talked with, that the framers of
the US export controls never envisioned that they would ever apply to
cryptographic freeware that has been published and scattered to the
winds.  It's hard to imagine a US attorney trying to build a real
case against someone for the "export" of software published freely in
the US.  As far as anyone I've talked to knows, it's never been done,
despite numerous examples of export violations.  I'm not a lawyer and
I'm not giving you legal advice-- I'm just trying to point out what
seems like common sense.

Starting with PGP version 2.0, the release point of the software has
been outside the US, on publicly-accessible computers in Europe.
Each release is electronically sent back into the US and posted on
publicly-accessible computers in the US by PGP privacy activists in
foreign countries.  There are some restrictions in the US regarding
the import of munitions, but I'm not aware of any cases where this
was ever enforced for importing cryptographic software into the US.
I imagine that a legal action of that type would be quite a spectacle
of controversy.

Some foreign governments impose serious penalties on anyone inside
their country for merely using encrypted communications.  In some
countries they might even shoot you for that.  But if you live in
that kind of country, perhaps you need PGP even more.

Computer-Related Political Groups

PGP is a very political piece of software.  It seems appropriate to
mention here some computer-related activist groups.  Full details on
these groups, and how to join them, is provided in a separate
document file in the PGP release package.

The Electronic Frontier Foundation (EFF) was founded in July, 1990,
to assure freedom of expression in digital media, with a particular
emphasis on applying the principles embodied in the Constitution and
the Bill of Rights to computer-based communication.  They can be
reached at:  Electronic Frontier Foundation, 238 Main Street,
Cambridge, MA 02142, USA.

The League for Programming Freedom (LPF) is a grass-roots organization
of professors, students, businessmen, programmers and users dedicated
to bringing back the freedom to write programs.  They regard patents
on computer algorithms as harmful to the US software industry.  They
can be reached at (617) 433-7071, or send Internet mail to
lpf@uunet.uu.net

Computer Professionals For Social Responsibility (CPSR) empowers
computer professionals and computer users to advocate for the
responsible use of information technology and empowers all who use
computer technology to participate in public policy debates on the
impacts of computers on society.  They can be reached at:
415-322-3778 in Palo Alto, E-mail address cpsr@csli.stanford.edu.

For more details on these groups, see the accompanying document in
the PGP release package.

Recommended Introductory Readings

1.  Dorothy Denning, "Cryptography and Data Security", Addison-Wesley,
    Reading, MA 1982
2.  Dorothy Denning, "Protecting Public Keys and Signature Keys",
    IEEE Computer, Feb 1983
3.  Martin E. Hellman, "The Mathematics of Public-Key Cryptography,"
    Scientific American, Aug 1979

Other Readings

4.  Ronald Rivest, "The MD5 Message Digest Algorithm", MIT Laboratory
    for Computer Science, 1991
5.  Xuejia Lai, "On the Design and Security of Block Ciphers",
    Institute for Signal and Information Processing, ETH-Zentrum,
    Zurich, Switzerland, 1992
6.  Xuejia Lai, James L. Massey, Sean Murphy, "Markov Ciphers and
    Differential Cryptanalysis", Advances in Cryptology- EUROCRYPT'91
7.  Philip Zimmermann, "A Proposed Standard Format for RSA
    Cryptosystems", Advances in Computer Security, Vol III, edited by
    Rein Turn, Artech House, 1988
8.  Paul Wallich, "Electronic Envelopes", Scientific American, Feb
    1993, pages 30-32.  (This is an article on PGP)

About the Author

Philip Zimmermann is a software engineer consultant with 18 years
experience, specializing in embedded real-time systems, cryptography,
authentication, and data communications.  Experience includes design
and implementation of authentication systems for financial
information networks, network data security, key management
protocols, embedded real-time multitasking executives, operating
systems, and local area networks.

Custom versions of cryptography and authentication products and
public key implementations such as the NIST DSS are available from
Zimmermann, as well as custom product development services.  His
consulting firm's address is:

Mr. Philip Zimmermann
Boulder Software Engineering
3021 Eleventh Street
Boulder, Colorado 80304  USA
Phone 303-541-0140 (voice or FAX)  (10:00am - 7:00pm Mountain Time)
Internet:  prz@sage.cgd.ucar.edu

Where to Get PGP

Finland:    nic.funet.fi  (128.214.6.100)
            Directory: /pub/unix/security/crypt/

Italy:      ghost.dsi.unimi.it  (149.132.2.1)
            Directory: /pub/security/

UK:         src.doc.ic.ac.uk
            Directory: /computing/security/software/PGP

For those lacking FTP connectivity to the net, nic.funet.fi also
offers the files via email.  Send the following mail message to
mailserv@nic.funet.fi:

    ENCODER uuencode
    SEND pub/unix/security/crypt/pgp22src.zip
    SEND pub/unix/security/crypt/pgp22.zip

This will deposit the two zipfiles, as (about) 15 batched messages in
your mailbox within about 24 hours.  Save and uudecode.

PGP is also widely available on Fidonet, a large informal network of
PC-based bulletin board systems interconnected via modems.  Check
your local bulletin board systems.  It is available on many foreign
and domestic Fidonet BBS sites.

For information on PGP implementations on the Apple Macintosh,
Commodore Amiga, or Atari ST, or any other questions about where to
get PGP for any other platform, contact Hugh Miller at
hmiller@lucpul.it.luc.edu.

Here are a few people and their email addresses or phone numbers you
can contact in some countries to get information on local PGP
availability:

Peter Gutmann                 Hugh Kennedy
pgut1@cs.aukuni.ac.nz         70042.710@compuserve.com
New Zealand                   Germany

Branko Lankester              Miguel Angel Gallardo
lankeste@fwi.uva.nl           gallardo@batman.fi.upm.es
+31 2159 42242                (341) 474 38 09
The Netherlands               Spain

Hugh Miller                   Colin Plumb
hmiller@lucpul.it.luc.edu     colin@nyx.cs.du.edu
(312) 508-2727                Toronto, Ontario, Canada
USA

Jean-loup Gailly (21 av. mary, 92500 Rueil Malmaison, Tel.: 1-47518065)
jloup@chorus.fr
France

------------------------------

End of Chaos Digest #1.18
************************************

Downloaded From P-80 International Information Systems 304-744-2253
