Chaos Digest               Lundi 22 Mars 1993          Volume 1 : Numero 15

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.15 (22 Mars 1993)
File 1--3e Ed. du Repert. des E-Journaux et Newsletters (Annonce)
File 2--Piratage a la francaise (reprint)
File 3--Vulnerabilite du finger Unix chez Commodore Amiga

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost from cccf@altern.com. The editors may be contacted by
voice (+33 1 47874083), fax (+33 1 47877070) or S-mail at: Jean-Bernard
Condat, Chaos Computer Club France [CCCF], B.P. 155, 93404 St-Ouen Cedex,
France

Issues of ChaosD can also be found on some French BBS. Back issues of
ChaosD can be found on the Internet as part of the Computer underground
Digest archives.  They're accessible using anonymous FTP from:

        * kragar.eff.org [192.88.144.4] in /pub/cud/chaos
        * uglymouse.css.itd.umich.edu [141.211.182.91] in /pub/CuD/chaos
        * halcyon.com [192.135.191.2] in /pub/mirror/cud/chaos
        * ftp.cic.net [192.131.22.2] in /e-serials/alphabetic/c/chaos-digest
        * ftp.ee.mu.oz.au [128.250.77.2] in /pub/text/CuD/chaos
        * nic.funet.fi [128.214.6.100] in /pub/doc/cud/chaos
        * orchid.csv.warwick.ac.uk [137.205.192.5] in /pub/cud/chaos

Issues of ChaosD can also be found on some French BBS. Back issues of
ChaosD can be found on the Internet as part of the Computer underground
Digest archives.  They're accessible using anonymous FTP from:
CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Thu Feb 11 13:17:49 EST 1993
From: 441495@acadvm1.uottawa.ca (MICHAEL STRANGELOVE )
Subject: File 1--3e Ed. du Repert. des E-Journaux et Newsletters (Annonce)


FYI/RFC - this will be released on the Net later next week - still editing
and tracking down some more hopefull leads. - Michael


 Introduction. . . . . . . . . . . . . . . . . . . . . . . . . .  9

 Reflections on Developments in Network-Based Electronic
      Serials: March 1992 -- March 1993. . . . . . . . . . . . . 12

 How To Retrieve This Directory From Networked Sources . . . . . 12
       Via Listserv. . . . . . . . . . . . . . . . . . . . . . . 12
       Via FTP . . . . . . . . . . . . . . . . . . . . . . . . . 13
       Accessing the Gopher/Telnet Server Version. . . . . . . . 13

 How To Subscribe to E-serials and Retrieve Back Issues. . . . . 13
       Subscribing to E-serials. . . . . . . . . . . . . . . . . 13
       Retrieving Back Issues. . . . . . . . . . . . . . . . . . 14
             Using FTP Mail Gateways . . . . . . . . . . . . . . 14
       Find Out More About LISTSERV. . . . . . . . . . . . . . . 15
       Retrieving E-Serials via FTP. . . . . . . . . . . . . . . 15
       Using FTP . . . . . . . . . . . . . . . . . . . . . . . . 16
       Connecting to a Node. . . . . . . . . . . . . . . . . . . 16
       Various Common FTP Commands and Their Use . . . . . . . . 17
       Find Out More About FTP . . . . . . . . . . . . . . . . . 18

 How To Submit an Entry to This Directory. . . . . . . . . . . . 18

 Networked Resources for Electronic Publication. . . . . . . . . 19
       Online Discussion Groups. . . . . . . . . . . . . . . . . 19
             Association of Electronic Discussion Groups and
                  Electronic Journals (ARACHNET) . . . . . . . . 19
             Association of Electronic Scholarly Journals
                  (AESJ-L) . . . . . . . . . . . . . . . . . . . 19
             Scholarly Electronic Journals and Electronic
                  Publishing Issues List . . . . . . . . . . . . 20
       Electronic Serials for Publishers and Editors . . . . . . 20
             CCNEWS - Campus Computing Newsletter. . . . . . . . 20
             EJOURNAL. . . . . . . . . . . . . . . . . . . . . . 20
       How to Start an E-Newsletter. . . . . . . . . . . . . . . 20
       Electronic Publishing on Networks: A Selective
            Bibliography of Recent Works . . . . . . . . . . . . 20
       Getting an ISSN for an Electronic Journal . . . . . . . . 25
       PACS Review Special Issue on Networked Based E-Serials. . 26

 Section 2:  Electronic Journals . . . . . . . . . . . . . . . . 28

 1. Arachnet Electronic Journal of Virtual Culture . . . . . . . 28

 2. Architronic: The Electronic Journal of Architecture. . . . . 29

 3. AXE. . . . . . . . . . . . . . . . . . . . . . . . . . . . . 29

 4. Bryn Mawr Classical Review . . . . . . . . . . . . . . . . . 30

 5. Catalyst: The Community Services Catalyst. . . . . . . . . . 31

 6. Distance Education Online Symposium (DEOSNEWS) . . . . . . . 32

 7. Education Policy Analysis Archives . . . . . . . . . . . . . 32

 8. EJOURNAL . . . . . . . . . . . . . . . . . . . . . . . . . . 33

 11. Flora Online. . . . . . . . . . . . . . . . . . . . . . . . 35

 12. Interpersonal Computing and Technology Journal: An
      Electronic Journal for the 21st Century. . . . . . . . . . 35

 13. IOUDAIOS Review . . . . . . . . . . . . . . . . . . . . . . 36

 14. Issues In Science and Technology Librarianship. . . . . . . 37

 15. Journal of Computing in Higher Education. . . . . . . . . . 37

 16. Journal of the International Academy of Hospitality
      Research . . . . . . . . . . . . . . . . . . . . . . . . . 38

 17. Journal of Technology Education . . . . . . . . . . . . . . 39

 18. LIBRES. . . . . . . . . . . . . . . . . . . . . . . . . . . 39

 19. MC Journal - Journal of Academic Media Librarianship. . . . 40

 20. MeckJournal . . . . . . . . . . . . . . . . . . . . . . . . 40

 21. Modal Analysis: The International Journal of Analytical and
      Experimental Modal Analysis. . . . . . . . . . . . . . . . 41

 22. Music Theory Online (MTO) . . . . . . . . . . . . . . . . . 42

 23. New Horizons in Adult Education . . . . . . . . . . . . . . 42

 24. Online Chronicle of Distance Education and Communication. . 43

 25. Online Journal of Current Clinical Trials . . . . . . . . . 44

 26. Postmodern Culture. . . . . . . . . . . . . . . . . . . . . 45

 27. Postmodern Jewish Philosophy BITNETWORK . . . . . . . . . . 46

 28. PSYCHE: An Interdisciplinary Journal of Research on
      Consciousness. . . . . . . . . . . . . . . . . . . . . . . 47

 29. Psychology Graduate Student Journal: The PSYCGRAD Journal
      (PSYGRD-J) . . . . . . . . . . . . . . . . . . . . . . . . 47

 30. PSYCOLOQUY. . . . . . . . . . . . . . . . . . . . . . . . . 48

 31. Public-Access Computer Systems Review . . . . . . . . . . . 49

 32. RD: Graduate Research in the Arts . . . . . . . . . . . . . 50

 33. Religious Studies Publications Journal - CONTENTS . . . . . 51

 34. SOLSTICE: An Electronic Journal of Geography and
      Mathematics. . . . . . . . . . . . . . . . . . . . . . . . 52

 35. Surfaces. . . . . . . . . . . . . . . . . . . . . . . . . . 53

 36. Textual Studies in Canada . . . . . . . . . . . . . . . . . 55

 Section 3:  Electronic Newsletters. . . . . . . . . . . . . . . 56

 37. Acquisitions Librarians Electronic Network (ACQNET) . . . . 56

 38. A & G Information Services. . . . . . . . . . . . . . . . . 56

 39. AIDS Book Review Journal. . . . . . . . . . . . . . . . . . 57

 40. ALCTS Network News (AN2). . . . . . . . . . . . . . . . . . 58

 41. Amazons International . . . . . . . . . . . . . . . . . . . 58

 42. American Psychological Association's Research Funding
      Bulletin . . . . . . . . . . . . . . . . . . . . . . . . . 59

 43. Andrew View . . . . . . . . . . . . . . . . . . . . . . . . 60

 44. Armadillo Culture: The Journal of Modern Dasypodidae. . . . 60

 45. Arm The Spirit. . . . . . . . . . . . . . . . . . . . . . . 61

 46. ART COM . . . . . . . . . . . . . . . . . . . . . . . . . . 61

 47. Automatome. . . . . . . . . . . . . . . . . . . . . . . . . 62

 48. Bean Bag. . . . . . . . . . . . . . . . . . . . . . . . . . 62

 49. BEN (Botanical Electronic News) . . . . . . . . . . . . . . 63

 50. Between the Lines . . . . . . . . . . . . . . . . . . . . . 64

 51. Biomedical Library Acquisitions Bulletin (BLAB) . . . . . . 64

 52. Braille Forum . . . . . . . . . . . . . . . . . . . . . . . 64

 53. Braille Monitor . . . . . . . . . . . . . . . . . . . . . . 65

 54. Buffer. . . . . . . . . . . . . . . . . . . . . . . . . . . 65

 55. Cache Update. . . . . . . . . . . . . . . . . . . . . . . . 65

 56. CACTUS Newsletter (Capital Area Central Texas Unix
      Society) . . . . . . . . . . . . . . . . . . . . . . . . . 66

 57. Carolina. . . . . . . . . . . . . . . . . . . . . . . . . . 66

 58. CCNEWS - Campus Computing Newsletter. . . . . . . . . . . . 66

 59. CERFNet NEWS. . . . . . . . . . . . . . . . . . . . . . . . 67

 60. Chaos Digest (ChaosD) . . . . . . . . . . . . . . . . . . . 67

 61. ChE Electronic Newsletter . . . . . . . . . . . . . . . . . 68

 62. Chile News Database . . . . . . . . . . . . . . . . . . . . 68

 63. China News Digest . . . . . . . . . . . . . . . . . . . . . 69

 65. Chronic Fatigue Syndrome Electronic Newsletter (CFS-NEWS) . 70

 66. Citations for Serial Literature . . . . . . . . . . . . . . 70

 67. Class Four Relay Magazine . . . . . . . . . . . . . . . . . 71

 68. Computer Science Center Link. . . . . . . . . . . . . . . . 72

 69. Computer Underground Digest (CuD or Cu-Digest). . . . . . . 73

 70. Computing and Network News. . . . . . . . . . . . . . . . . 74

 71. Computists' Communique. . . . . . . . . . . . . . . . . . . 74

 72. Consortium Update . . . . . . . . . . . . . . . . . . . . . 75

 73. CORE. . . . . . . . . . . . . . . . . . . . . . . . . . . . 75

 74. Cosmic Update . . . . . . . . . . . . . . . . . . . . . . . 76

 75. CPSR/PDX Newsletter . . . . . . . . . . . . . . . . . . . . 76

 76. CRTNet - Communication Research and Theory Network. . . . . 77

 77. ctt-Digest: The comp.text.tex Newsgroup Digest. . . . . . . 77

 78. Cult of the Dead Cow. . . . . . . . . . . . . . . . . . . . 78

 79. Current Cites . . . . . . . . . . . . . . . . . . . . . . . 79

 80. Cyberspace Vanguard Magazine. . . . . . . . . . . . . . . . 79

 81. Dargonzine - The Magazine of the Dargon Project . . . . . . 80

 82. DATA ENTRIES. . . . . . . . . . . . . . . . . . . . . . . . 80

 83. Dateline: Starfleet . . . . . . . . . . . . . . . . . . . . 81

 84. DDN Management Bulletin . . . . . . . . . . . . . . . . . . 81

 85. DECNEWS for Education and Research. . . . . . . . . . . . . 82

 86. Deutschland Nachrichten . . . . . . . . . . . . . . . . . . 82

 87. DevelopNet News . . . . . . . . . . . . . . . . . . . . . . 82

 88. Digital Games Review. . . . . . . . . . . . . . . . . . . . 83

 89. Disaster Research . . . . . . . . . . . . . . . . . . . . . 83

 90. Donosy. . . . . . . . . . . . . . . . . . . . . . . . . . . 84

 91. Drosophila Information Newsletter . . . . . . . . . . . . . 85

 92. EFFector Online - The Electronic Frontier Foundation, Inc . 85

 93. Electronic AIR. . . . . . . . . . . . . . . . . . . . . . . 86

 94. Electronic Hebrew Users Newsletter (E-Hug). . . . . . . . . 86

 95. End Process . . . . . . . . . . . . . . . . . . . . . . . . 87

 96. Energy Ideas. . . . . . . . . . . . . . . . . . . . . . . . 87

 97. Energy Research in Israel Newsletter. . . . . . . . . . . . 88

 98. Erofile . . . . . . . . . . . . . . . . . . . . . . . . . . 88

 99. Ethnomusicology Research Digest . . . . . . . . . . . . . . 89

 100. EUVE: Electronic Newsletter of the EUVE Observatory. . . . 89

 101. FactSheet Five - Electric. . . . . . . . . . . . . . . . . 90

 102. FARNET Gazette . . . . . . . . . . . . . . . . . . . . . . 90

 103. Fineart Forum. . . . . . . . . . . . . . . . . . . . . . . 91

 104. Fine Art, Science and Technology News (F.A.S.T. News). . . 91

 105. FOREFRONTS . . . . . . . . . . . . . . . . . . . . . . . . 91

 106. Fulbright Educational Advising Newsletter (FULBNEWS) . . . 92

 107. FutureCulture FAQ. . . . . . . . . . . . . . . . . . . . . 93

 108. GLOSAS News (GLObal Systems Analysis and Simulating
      Association) . . . . . . . . . . . . . . . . . . . . . . . 93

 109. GNET - Global Networking . . . . . . . . . . . . . . . . . 94

 110. GNU's Bulletin:  Newsletter of the Free Software
      Foundation . . . . . . . . . . . . . . . . . . . . . . . . 94

 111. The Handicap Digest. . . . . . . . . . . . . . . . . . . . 95

 112. HICNet Newsletter (MEDNEWS - The Health InfoCom
      Newsletter). . . . . . . . . . . . . . . . . . . . . . . . 96

 113. High Weirdness by Email. . . . . . . . . . . . . . . . . . 96

 114. Holy Temple of Mass Consumption. . . . . . . . . . . . . . 97

 115. Hot Off the Tree (HOTT). . . . . . . . . . . . . . . . . . 97

 116. ICS Electrozine: Information, Control, Supply. . . . . . . 98

 117. IHOUSE-L International Voice Newsletter Prototype List . . 98

 118. IMPACT ONLINE. . . . . . . . . . . . . . . . . . . . . . . 99

 119. Information Networking News. . . . . . . . . . . . . . . . 99

 120. Instant Math Preprints (IMP) . . . . . . . . . . . . . . .100

 121. Intertext - An Electronic Fiction Magazine . . . . . . . .101

 122. Internet Monthly Report. . . . . . . . . . . . . . . . . .101

 123. IR-LIST Digest (IR-L Digest) . . . . . . . . . . . . . . .102

 124. I.S.P.O.B. Bulletin YSSTI. . . . . . . . . . . . . . . . .102

 125. Jonathan's Space Report. . . . . . . . . . . . . . . . . .103

 126. KIDLINK Newsletter . . . . . . . . . . . . . . . . . . . .103

 127. Laboratory Primate Newsletter. . . . . . . . . . . . . . .104

 128. Law and Politics Book Review . . . . . . . . . . . . . . .104

 129. Leonardo Electronic News . . . . . . . . . . . . . . . . .105

 130. Link Letter. . . . . . . . . . . . . . . . . . . . . . . .105

 131. List Review Service. . . . . . . . . . . . . . . . . . . .106

 132. LymeNet Newsletter . . . . . . . . . . . . . . . . . . . .106

 133. MAB Northern Sciences Network Newsletter . . . . . . . . .107

 134. Material Science in Israel Newsletter. . . . . . . . . . .107

 135. Matrix News. . . . . . . . . . . . . . . . . . . . . . . .108

 136. MichNet News (previously Merit Network News) . . . . . . .108

 137. MICnews. . . . . . . . . . . . . . . . . . . . . . . . . .109

 138. Navy News Service (NAVNEWS). . . . . . . . . . . . . . . .109

 139. NEARnet Newsletter . . . . . . . . . . . . . . . . . . . .110

 140. NEARnet this Month . . . . . . . . . . . . . . . . . . . .110

 141. NetMonth . . . . . . . . . . . . . . . . . . . . . . . . .111

 142. Network Audio Bits and Audio Software Review . . . . . . .111

 143. Network News . . . . . . . . . . . . . . . . . . . . . . .112

 144. Newsbrief. . . . . . . . . . . . . . . . . . . . . . . . .112

 145. Newsletter on Serials Pricing Issues . . . . . . . . . . .113

 146. Newsline . . . . . . . . . . . . . . . . . . . . . . . . .113

 147. NIBNews - A Monthly Electronic Bulletin About Medical
      Informatics. . . . . . . . . . . . . . . . . . . . . . . .114

 148. NLSNews Newsletter . . . . . . . . . . . . . . . . . . . .115

 149. OFFLINE. . . . . . . . . . . . . . . . . . . . . . . . . .115

 150. Old English Computer-Assisted Language Learning Newsletter
      (OE-CALL). . . . . . . . . . . . . . . . . . . . . . . . .116

 151. Organized Thoughts . . . . . . . . . . . . . . . . . . . .116

 152. PIGULKI. . . . . . . . . . . . . . . . . . . . . . . . . .117

 153. Political Analysis and Research Cooperation (PARC) News
      Bulletin . . . . . . . . . . . . . . . . . . . . . . . . .118

 154. Practical Anarchy Online . . . . . . . . . . . . . . . . .118

 155. Principia Cybernetica Newsletter . . . . . . . . . . . . .118

 156. Prompt . . . . . . . . . . . . . . . . . . . . . . . . . .119

 157. Project Gutenberg Newsletter . . . . . . . . . . . . . . .120

 158. Public-Access Computer Systems News. . . . . . . . . . . .120

 159. Purple Thunderbolt of Spode (PURPS). . . . . . . . . . . .121

 160. QUANTA - Science, Fact, and Fiction. . . . . . . . . . . .121

 161. REACH - Research and Educational Applications of Computers
      in the Humanities. . . . . . . . . . . . . . . . . . . . .122

 162. ReNews (RELCOM NEWS) . . . . . . . . . . . . . . . . . . .123

 163. Rezo, bulletin irregulomadaire du RQSS . . . . . . . . . .123

 164. RFE/RL Research Institute Daily Report . . . . . . . . . .124

 165. RFE/RL Research Institute Research Bulletin. . . . . . . .124

 166. Risks-Forum Digest . . . . . . . . . . . . . . . . . . . .125

 167. RSI Network Newletter. . . . . . . . . . . . . . . . . . .125

 168. SCHOLAR. . . . . . . . . . . . . . . . . . . . . . . . . .126

 169. Scream Baby. . . . . . . . . . . . . . . . . . . . . . . .127

 170. SCUP BITNET NEWS . . . . . . . . . . . . . . . . . . . . .127

 171. SCUPMA-L: Society of College and University Planners,
      Mid-Atlantic Region. . . . . . . . . . . . . . . . . . . .128

 172. Sense of Place . . . . . . . . . . . . . . . . . . . . . .128

 173. Simulation Digest. . . . . . . . . . . . . . . . . . . . .129

 174. Simulations Online . . . . . . . . . . . . . . . . . . . .129

 175. Socjety Journal. . . . . . . . . . . . . . . . . . . . . .130

 176. Somalia News Update. . . . . . . . . . . . . . . . . . . .130

 177. SOUND News and Arts. . . . . . . . . . . . . . . . . . . .131

 178. Sound Newsletter . . . . . . . . . . . . . . . . . . . . .131

 179. South Florida Environmental Reader . . . . . . . . . . . .132

 180. South Scanner Satellite Services Chart . . . . . . . . . .132

 181. SpaceViews . . . . . . . . . . . . . . . . . . . . . . . .133

 182. SunFlash . . . . . . . . . . . . . . . . . . . . . . . . .133

 183. SURFPUNK Technical Journal . . . . . . . . . . . . . . . .133

 184. TapRoot Reviews Electronic . . . . . . . . . . . . . . . .134

 187. Temptation of Saint Anthony. . . . . . . . . . . . . . . .136

 190. Terminometro Electronico . . . . . . . . . . . . . . . . .137

 191. TeXMaG . . . . . . . . . . . . . . . . . . . . . . . . . .138

 192. TeX Publication Distribution List. . . . . . . . . . . . .139

 193. TidBITS. . . . . . . . . . . . . . . . . . . . . . . . . .140

 194. TREK-REVIEW-L. . . . . . . . . . . . . . . . . . . . . . .140

 195. Tunisian Scientific Society Newsletter . . . . . . . . . .140

 198. University of Missouri - Columbia Campus Computing
      Newsletter . . . . . . . . . . . . . . . . . . . . . . . .142

 199. Unplastic News . . . . . . . . . . . . . . . . . . . . . .142

 200. Week in Germany. . . . . . . . . . . . . . . . . . . . . .142

 201. World View Magazine. . . . . . . . . . . . . . . . . . . .143

 Section Three:
Selected Network Guides and Useful Information Files . . . . . .143

       General Information Documents About the Net . . . . . . .143
       Selected Network User Guides. . . . . . . . . . . . . . .146
       System-Specific Guides. . . . . . . . . . . . . . . . . .147
       Print Books About the Net . . . . . . . . . . . . . . . .148
       Print Magazines About the Net . . . . . . . . . . . . . .149
       Hypertext Guides to the Net . . . . . . . . . . . . . . .150


Michael Strangelove
Department of Religious Studies
University of Ottawa

         BITNET: 441495@Uottawa
         Internet: 441495@Acadvm1.Uottawa.CA
         S-Mail: 177 Waller, Ottawa, Ontario, K1N 6N5 CANADA
         Voice:  (613) 747-0642
         FAX:    (613) 564-6641

[MODERATEUR: Pour obtenir une version de ce repertoire, il suffit d'envoyer
a  LISTSERV@acadvm1.uottawa.ca, un message contenant les deux lignes:
         get ejournl1 directry
         get ejournl2.directry
Vous obtiendrez alors deux fichiers ASCII de 109821 et 132937 caracteres.]

------------------------------

Date: Mon Mar 22 10:16:00 EST 1993
From: patt@SQUID.TRAM.COM (Patt Bromberger )
Subject: File 2--Piratage a la francaise (reprint)
Copyright: Paul Loubiere, Liberation, 1993


                        La Micro sourit aux pirates

Les entreprises ont de plus en plus de mal a verouiller leurs systemes: l'an
dernier, le cout du piratage s'est eleve a plusieurs milliards de francs.

     "Tiens, ils ont oublie de verouiller mes acces... Je vais visiter. "La
commence la mesaventure d'un cadre, appelons-le Xavier, tout juste embauche
dans une grande compagnie d'assurances.  On lui montre son bureau, son
ordinateur relie aux autres machines de l'entreprise, et on le presente aux
membres du personnel.  Le pot de bienvenue est un succes, tout s'annonce
pour le mieux.  C'est alors que le diable qui sommeille dans chaque puce
decide de frapper.  D'abord, on oublie de verouiller les acces de Xavier aux
donnees sensibles du systeme informatique: il se retrouve dans la situation
d'un employe a qui on aurait donne un passe-partout lui permettant d'ouvrir
toutes les portes.  Mais voila, Xavier est aussi, a ses moments perdus, un
amateur de bits et d'octets.  Il commence a explorer le systeme.  Au hasard
de ses peregrinations, il tombe sur les feuilles de payes de la direction.
Et ne peut resister a l'envie de les imprimer puis de les placarder sur les
murs de l'entreprise.  La plaisanterie ne fut pas du gout de tout le monde,
et Xavier fut sechement remercie.

     Cas exemplaire mais isole?  Pas tout a fait. Temoin, l'affluence au
salon Infosec, qui se tenait la semaine derniere au Cnit de La Defense et
proposait toute une gamme de solutions pour eviter ce genre de desagrements.
Car la securite n'a pas suivi les progres de l'informatique.  Bien rodee a
la fin des annees 80, elle est en recul depuis trois ans.  Et pas a cause
de la negligence des utilisateurs ou des responsables.  Le mal evolue avec
l'informatique: les entreprises veulent maintenant des ordinateurs relies
entre eux par des reseaux locaux qui ne sont plus organises autour d'un
grand systeme.

     "Autrefois, c'etait un peu l'epoque sovietique, plaisante Olivier
Lecorre, un directeur informatique.  Un ordinateur central controlait tout.
Rien ne pouvait entrer ou sortir de la machine sans en referer a l'autorite."
Pas question pour un non-specialiste de s'aprocher d'un ecran
ou d'un clavier.  Les perturbations commencent avec l'arrivee de la micro-
informatique.  "Tout d'un coup, l'omnipotence du technicien etait battue en
breche, reprend Olivier Lecorre.  N'importe quel utilisateur faisait ce
qu'il voulait!".  Tout change encore quand la puissance des micros commence
a concurencer celle des grosses machines.

     Et pourtant, l'evolution est inevitable.  "C'est un peu le probleme de
la contrebande, remarque un specialiste de la securite.  Si les frontieres
sont etanches, c'est qu'il n'y a pas d'echanges avec l'exterieur.  Au
contraire, plus le commerce augmente, plus il est facile de faire passer
n'importe quoi d'un pays a l'autre.  C'est pareil ici: l'augmentation des
echanges informatiques produit mecaniquement une augmentation des risques."
En outre, la communication avec l'exterieur produit un deuxieme phenomene:
l'informatique de papa, avec son grand systeme et ses machines enfermees
dans des bunkers accessibles a un petit nombre d'employes pouvait etre
efficacement centralisee.  Mais la communication est aujourd'hui globale,
avec tout un chacun, par le truchement de reseaux de fibre optique, du
telephone et du modem hertzien (qui permet d'expedier des donnees
informatiques alors qu'on pianote sur son portable confortablement vautre
dans son siege de TGV).  "On peut structurer la police dans un seul pays,
soupire le specialiste de la securite, mais comment voulez-vous organiser
une police mondiale?  Resultat: des systemes differents, avec des regles de
securite differents--parfois incompatibles--sont connectes les uns avec les
autres.  Bref, c'est la pagaille."

     Une pagaille qui coute cher.  La France a perdu 10 milliards de francs
en 1992 a cause de son informatique, d'apres le Club de la securite
informatique francais (Clusif).  Les erreurs d'utilisation (saisie et
transmission des donnees) ont coute 1,9 milliards (18% du total).  Les
accidents (incendie, explosion, implosion...) ont coute 2,62 milliards (25%).
Enfin, la malveillance (vol, utilisation non autorisee des ressources
du systeme pouvant conduire a des detournements de fonds, sabotage) se
taille la part du lion avec 5,9 milliards (57%).

     "C'est simple, explique un informaticien de Thomson Digital Image, je
debranche systematiquement mon modem quand je ne l'utilise pas.  Sinon, je
risque de me faire piquer les informations contenues dans ma station de
travail."  La fraude est simple: il suffit de se connecter par telephone
avec l'ordinateur qu'on veut visiter.  Avec un peu d'habilete, on peut
parfois arriver a penetrer dans le systeme et a voler les informations qu'il
contient.  Des entreprises ont ainsi perdu des marches a cause du vol de la
liste des clients.  D'autres ont rate des marches parce qu'un concurent peu
scrupuleux a fait disparaitre des donnees cruciales. Le miracle de l'echange
de donnees peut aussi etre responsable de catastrophe. Un promoteur avait
l'habitude d'expedier electroniquement ses commandes de materiel.  Un
saboteur a maquille les instructions concernant le choix fait par les
clients des types de moquette a poser.  Bilan: 1,5 million de francs de
pertes.

------------------------------

Date: Thu Feb 18 10:51:20 EST 1993
From: cert-advisory-request@cert.org (CERT Advisory )
Subject: File 3--Vulnerabilite du finger Unix chez Commodore Amiga

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CA-93:04a                     CERT Advisory
                            February 18, 1993
      REVISION NOTICE: Commodore Amiga UNIX finger Vulnerability
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

               *** THIS IS A REVISED CERT ADVISORY ***
               *** IT CONTAINS UPDATED INFORMATION ***

The CERT Coordination Center has received information concerning a
vulnerability in the "finger" program of Commodore Business Machine's
Amiga UNIX product.  The vulnerability affects Commodore Amiga UNIX
versions 1.1, 2.03, 2.1, 2.1p1, 2.1p2, and 2.1p2a.  Commodore is aware
of the vulnerability, and both a workaround and a patch are available.
Affected sites should apply either the workaround or the patch, and
directions are provided below.

The Commodore contact e-mail address given in CERT Advisory CA-93:04
was incorrect.  This revised advisory provides the correct e-mail
address.  If you have any further questions, contact David Miller of
Commodore via e-mail at davidm@commodore.com.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

I.    Description

      The "finger" command in Amiga UNIX contains a security
      vulnerability.


II.   Impact

      Non-privileged users can gain unauthorized access to files.


III.  Solution

      Commodore has suggested a workaround and a patch, as follows:

      A. Workaround

         As root, modify the permission of the existing /usr/bin/finger
         to prevent misuse.

                # /bin/chmod 0755 /usr/bin/finger

      B. Patch

         As root, install the "pubsrc" package from the distribution tape.

         In the file, "/usr/src/pub/cmd/finger/src/finger.c", add the line:

                setuid(getuid());

         immediately before the line reading:

                display_finger(finger_list);

         (Optionally) save a copy of the existing /usr/bin/finger and modify
         its permission to prevent misuse.

                # /bin/mv /usr/bin/finger /usr/bin/finger.orig
                # /bin/chmod 0755 /usr/bin/finger.orig

         In the directory, "/usr/src/pub/cmd/finger", issue the command:

                # cd /usr/src/pub/cmd/finger
                # make install
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
The CERT Coordination Center wishes to thank Commodore Business
Machines for their response to this problem.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

CERT Coordination Center
Software Engineering Institute
Carnegie Mellon University
Pittsburgh, PA 15213-3890

Internet E-mail: cert@cert.org
Telephone: 412-268-7090 (24-hour hotline)
           CERT personnel answer 7:30 a.m.-6:00 p.m. EST(GMT-5)/EDT(GMT-4),
           on call for emergencies during other hours.

[MODERATEUR: J'ai essaye de joindre le P-DG de Commodore France, M. Georges
Fournay. M. Nicolas Costes nous a confesse que "ce probleme etait deja connu
et que l'etat major europeen avait ete averti.  Toutes les machines seront
desormais modifiees, mais il n'y a rien a craindre pour l'instant.  Aucun
incident n'a jamais eclate sur l'Unix, ce qui nous a permis de supprimer le
poste de responsable Unix.  Si nous avons parler d'Unix, ce n'etait que pour
une raison publicitaire.  Enfin, la solution du CERT est mauvaise:
desactiver le finger de la sorte, c'est catastrophique!"

------------------------------

End of Chaos Digest #1.15
************************************

Downloaded From P-80 International Information Systems 304-744-2253
