**********************************************************************
DDN Security Bulletin 04         DCA DDN Defense Communications System
23 Oct 89               Published by: DDN Security Coordination Center
                                     (SCC@NIC.DDN.MIL)  (800) 235-3155
 
                        DEFENSE  DATA  NETWORK
                          SECURITY  BULLETIN
 
The DDN  SECURITY BULLETIN  is distributed  by the  DDN SCC  (Security
Coordination Center) under  DCA contract as  a means of  communicating
information on network and host security exposures, fixes, &  concerns
to security & management personnel at DDN facilities.  Back issues may
be  obtained  via  FTP  (or  Kermit)  from  NIC.DDN.MIL  [26.0.0.73 or
10.0.0.51] using login="anonymous" and password="guest".  The bulletin
pathname is SCC:DDN-SECURITY-nn (where "nn" is the bulletin number).
 
**********************************************************************
 
                     HALLOWEEN PRECAUTIONARY NOTE
 
Halloween is traditionally a time  for tricks of all kinds.   In order
to guard against possible benign or malevolent attempts to affect  the
normal operation of your host,  the DDN SCC staff suggests  taking the
following easy precautions:
 
   1. Write a set of emergency procedures for your site and keep it up
      to date.  Address such things as:
 
         - What would you do if you had an intruder (either a human or
           a computer virus)?
 
         - Who would you  call for help?  HINT:  Read the top  of this
           bulletin!  Also, for 24 hour assistance:
 
           MILNET Trouble Desk -- (A/V) 231-1713 or (800) 451-7413
 
         - Who is in charge of security at your site?
 
         - How would you apply a hardware/software fix if needed?
 
   2. Save your files regularly,  and make file  back-ups often.   Put
      the distribution copies of your  software in  a safe  place away
      from your computer room.  Don't forget where they're stored!
 
   3. Avoid trivial passwords and change them often.   (See the "Green
      Book"  (Department  of  Defense  Password Management Guideline),
      CSC-STD-002-85, for information on the use of passwords.)
 
   4. Check  to  make  sure  your  host  has no  unauthorized users or
      accounts.  Also check for obsolete accounts (a favorite path for
      intruders to gain access).
 
   5. Restrict system  ("superuser", "maint", etc.)  privileges to the
      minimum number of accounts you possibly can.
 
   6. Well publicized accounts including "root", "guest", etc. AND the
      personal account  for the  system administrator  should NOT have
      system privileges.   (Past experience  has shown  that these IDs
      are more susceptible to successful intruder attacks.)
 
   7. Keep your maintenance contracts active.
 
Of course,  these steps should be taken throughout the year as part of
your regular operating procedure.
 
**********************************************************************
X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X
 Another file downloaded from:                                NIRVANAnet(tm)

 &TOTSE                510/935-5845   Walnut Creek, CA         Taipan Enigma
 Burn This Flag        408/363-9766       San Jose, CA                Zardoz
 realitycheck          415/666-0339  San Francisco, CA    Poindexter Fortran
 Governed Anarchy      510/226-6656        Fremont, CA             Eightball
 New Dork Sublime      805/823-1346      Tehachapi, CA               Biffnix
 Lies Unlimited        801/278-2699 Salt Lake City, UT            Mick Freen
 Atomic Books          410/669-4179      Baltimore, MD               Baywolf
 Sea of Noise          203/886-1441        Norwich, CT             Mr. Noise
 The Dojo              713/997-6351       Pearland, TX               Yojimbo
 Frayed Ends of Sanity 503/965-6747     Cloverdale, OR              Flatline
 The Ether Room        510/228-1146       Martinez, CA Tiny Little Super Guy
 Hacker Heaven         860/456-9266        Lebanon, CT         The Visionary
 The Shaven Yak        510/672-6570        Clayton, CA             Magic Man
 El Observador         408/372-9054        Salinas, CA         El Observador
 Cool Beans!           415/648-7865  San Francisco, CA        G.A. Ellsworth
 DUSK Til Dawn         604/746-5383   Cowichan Bay, BC         Cyber Trollis
 The Great Abyss       510/482-5813        Oakland, CA             Keymaster

                          "Raw Data for Raw Nerves"
X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X
