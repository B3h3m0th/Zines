
-----BEGIN PGP SIGNED MESSAGE-----

=============================================================================
CERT(sm) Advisory CA-96.10                     
Original issue date: May 28, 1996
Last revised: August 30, 1996
              Information previously in the README was inserted
              into the advisory.

              A complete revision history is at the end of this file.

Topic:  NIS+ Configuration Vulnerability
- -----------------------------------------------------------------------------

   The text of this advisory was originally released by the Australian
   Emergency Response Team on May 20, 1996, and updated on May 27, 1996, as
   AUSCERT advisory AA-96.02a.

   Because of the seriousness of the problem, we are reprinting the AUSCERT
   advisory here with their permission. Only the contact information at the
   end has changed: AUSCERT contact information has been replaced with
   CERT/CC contact information. 

   We will update this advisory as we receive additional information.
   Please check advisory files regularly for updates that relate to your site.

=============================================================================

AUSCERT has received information that a vulnerability exists under some
configurations of NIS+.  In vulnerable installations of NIS+, the access
rights on the NIS+ passwd table are left in an unsecure state.

This vulnerability is known to exist in NIS+ installations initially created
on Solaris 2.5 servers.  Similar vulnerabilities in NIS+ configurations may
also exist in previous versions of Solaris 2.

This vulnerability may allow any user with valid NIS+ credentials to gain
root privileges.

AUSCERT recommends that any site which has NIS+ installed take this
opportunity to check their installations and apply the appropriate 
workarounds as described in Section 3.

** This updated advisory contains clarifications for sites requiring password
** aging facilities and sites running their NIS+ servers in NIS compatibility
** mode.

- -----------------------------------------------------------------------------

1.  Description

    NIS+ provides distributed network access to information sources such as
    password, group and host information.  It maintains this information in
    the form of NIS+ tables.  NIS+ tables contain the administrative
    information normally supplied by local files (such as /etc/passwd).  As
    with the standard Unix administration files, setting secure permissions
    on the NIS+ tables is of utmost importance in maintaining system 
    security.

    NIS+ provides a comprehensive set of access rights for NIS+ tables.  This
    includes permissions not only on NIS+ tables but also individual columns
    and entries in those tables.  Due to the added complexity, sites need to
    be particularly diligent in ensuring that permissions on NIS+ tables (and
    associated entries and columns) are secure.

    AUSCERT encourages sites running NIS+ to gain a good understanding of
    the permission model used by NIS+.  A complete description may be found
    in the NIS+ documentation set.  The rest of this advisory assumes a good
    understanding of NIS+ permission controls.

    AUSCERT has received information that under some installations of NIS+
    the permissions on the NIS+ passwd table are left in an unsecure state.

    This vulnerability is known to exist in NIS+ installations initially
    created on Solaris 2.5 servers.  Similar vulnerabilities in NIS+
    configurations may also exist in previous versions of Solaris 2.

2.  Impact

    Any user with login access to a client or server that uses NIS+ for
    authentication may gain root privileges.

3.  Workarounds

    NIS+ uses an access control mechanism for granting access to NIS+ tables
    which is similar (but not identical) to that used by the standard Unix
    file system.  NIS+ tables are assigned permissions for the NIS+ user
    categories nobody, owner, group and world.  NIS+ also has permissions
    associated with columns and individual entries in NIS+ tables.

    Under some installations of NIS+ the permissions of the NIS+ passwd
    table and its columns are left in an unsecure state.  These permissions
    can be viewed using niscat(1).

    To check the permissions on the NIS+ passwd table, sites can use:

    # niscat -o passwd.org_dir

    This should produce output similar to:

Object Name   : passwd
Owner         : myhost.mydomain.org.
Group         : admin.mydomain.org.
Domain        : org_dir.mydomain.org.
Access Rights : ----rmcdrmcd----
Time to Live  : 12:0:0
Object Type   : TABLE
Table Type          : passwd_tbl
Number of Columns   : 8
Character Separator : :
Search Path         :
Columns             :
        [0]     Name          : name
                Attributes    : (SEARCHABLE, TEXTUAL DATA, CASE SENSITIVE)
                Access Rights : r---------------
        [1]     Name          : passwd
                Attributes    : (TEXTUAL DATA)
                Access Rights : -----m----------
        [2]     Name          : uid
                Attributes    : (SEARCHABLE, TEXTUAL DATA, CASE SENSITIVE)
                Access Rights : r---------------
        [3]     Name          : gid
                Attributes    : (TEXTUAL DATA)
                Access Rights : r---------------
        [4]     Name          : gcos
                Attributes    : (TEXTUAL DATA)
                Access Rights : r---------------
        [5]     Name          : home
                Attributes    : (TEXTUAL DATA)
                Access Rights : r---------------
        [6]     Name          : shell
                Attributes    : (TEXTUAL DATA)
                Access Rights : r---------------
        [7]     Name          : shadow
                Attributes    : (TEXTUAL DATA)
                Access Rights : ----------------

    This output shows two types of access rights associated with the NIS+
    passwd table.  First, the default access rights for the table, which are
    given at the start of the output (----rmcdrmcd----).  Second,  the access
    rights associated with each column.

    In particular, sites should check the access rights on the columns of
    the NIS+ passwd table.  It should be noted that it appears that individual
    entries of the passwd table are owned by individual users.  The above
    access rights do not allow a user to modify any part of their passwd
    table entry besides their own passwd field.  For many environments this
    is acceptable.  

    However, depending on the local site configuration and requirements,
    additional access rights may also be needed.

    - Sites that wish users to be able to change their shell or gcos
    information may have the (m)odify bit set for owner on the shell or gcos
    column as needed.

    - Sites that have their NIS+ servers running in NIS compatibility
    mode to serve NIS clients may require (r)ead permission for nobody on the
    NIS+ passwd table.

    - Sites that are using password aging may require additional access
    rights on the shadow column.  The exact access rights will depend on the
    particular NIS+ version (including patches).  Sites are encouraged to
    check their local documentation for more information.

    Other than this, the access rights on columns should appear as shown in
    the niscat(1) output above.

    Any additional access rights on the table or its columns besides those
    shown above may allow a user to gain additional privileges, including
    possibly root.  Sites should completely understand the ramifications if
    they allow additional access rights.

    Sites may set the access rights on the NIS+ passwd table, as shown in the
    above output, by issuing the following commands as root on the master 
    NIS+ server.

    To set the default access rights for the NIS+ passwd table:

        # nischmod na-rmcd,og+rmcd passwd.org_dir

    To set the column access rights on the NIS+ passwd table:

        # nistbladm -u name=na-rmcd,n=r passwd.org_dir
        # nistbladm -u passwd=na-rmcd,o=m passwd.org_dir
        # nistbladm -u uid=na-rmcd,n=r passwd.org_dir
        # nistbladm -u gid=na-rmcd,n=r passwd.org_dir
        # nistbladm -u gcos=na-rmcd,n=r passwd.org_dir
        # nistbladm -u home=na-rmcd,n=r passwd.org_dir
        # nistbladm -u shell=na-rmcd,n=r passwd.org_dir
        # nistbladm -u shadow=na-rmcd passwd.org_dir

    After making any changes in access rights, AUSCERT recommends that sites
    verify the changes they have made using niscat(1), as shown previously.

    Sites that have replica NIS+ servers may use nisping(1m) to propagate
    the changes to the replica servers in a timely manner.

4.  Additional measures

    AUSCERT recommends that sites take this opportunity to ensure that all
    NIS+ tables have access rights in accordance with the local site security
    policy.  This also includes checking access rights on all the columns
    and entries of the NIS+ tables in addition to the default access rights
    of the tables themselves.

- -----------------------------------------------------------------------------
AUSCERT wishes to thank Ivan Angus and David Clarke of ANU for reporting this
vulnerability and for their advice in the preparation of this advisory.
AUSCERT also acknowledges Marek Krawus of UQ,  Reinhard Uebel and Mark
McPherson of QTAC for their assistance.
- -----------------------------------------------------------------------------

The AUSCERT team have made every effort to ensure that the information
contained in this document is accurate.  However, the decision to use the
information described is the responsibility of each user or organisation.
The appropriateness of this document for an organisation or individual system
should be considered before application in conjunction with local policies
and procedures.  AUSCERT takes no responsibility for the consequences of
applying the contents of this document.

=============================================================================

CERT Contact Information
- ------------------------

If you believe that your system has been compromised, contact the CERT
Coordination Center or your representative in the Forum of Incident
Response and Security Teams (FIRST).

We strongly urge you to encrypt any sensitive information you send by email.
The CERT Coordination Center can support a shared DES key and PGP. Contact 
the CERT staff for more information. 

Location of CERT PGP key
         ftp://info.cert.org/pub/CERT_PGP.key


Email    cert@cert.org

Phone    +1 412-268-7090 (24-hour hotline)
                CERT personnel answer 8:30-5:00 p.m. EST
                (GMT-5)/EDT(GMT-4), and are on call for
                emergencies during other hours.

Fax      +1 412-268-6989

Postal address
        CERT Coordination Center
        Software Engineering Institute
        Carnegie Mellon University
        Pittsburgh PA 15213-3890
        USA

CERT publications, information about FIRST representatives, and other
security-related information are available for anonymous FTP from
        http://www.cert.org/
        ftp://info.cert.org/pub/

CERT advisories and bulletins are also posted on the USENET newsgroup
        comp.security.announce

To be added to our mailing list for CERT advisories and bulletins, send your
email address to
        cert-advisory-request@cert.org


Copyright 1996 Carnegie Mellon University
This material may be reproduced and distributed without permission provided
it is used for noncommercial purposes and the copyright statement is 
included.

CERT is a service mark of Carnegie Mellon University.

==============================================================================
UPDATES

CERT/CC received information concerning an additional problem with the
ROW access rights in the NIS+ password table.  Accounts created on
Solaris 2.4 and 2.5 systems have excessive rights on the system.
These new super accounts have read, modify, create, and delete access
rights on their own rows in the nisplus password table.  This means
they can alter all attributes on their own entries.

To determine if your system is so affected, execute the following:

        % niscat -o '[name=juke],passwd.org_dir' | egrep "Access"

If the output displays information similar to the following:

          Access Rights : ----rmcdr---r---
                              ^^^^
then the owner can read, modify, change, and delete information.

The rights at this level should be more restrictive, and the individual rights
on entries should be less restrictive.  The less restrictive rights on entries
allow a user to change their password entry, the GECOS field, and even the
shell depending upon how the entry rights are set.

The output from the niscat above should look like the following:

          Access Rights : ----r-----------

This allows only the user to read information from the password table.

One way to determine which entries in the password table need to be changed is
the following:

        % niscat -o '[ ],passwd.org_dir' | egrep "Owner|rmc"

To fix the entries, use the following:

          % nischmod o=r,ngw-rmdc '[ ],passwd.org_dir'

This sets the owner permissions to r (read) and removes all permissions from
nobody, group, and world.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Revision history

Aug. 30, 1996  Information previously in the README was inserted into the
               advisory.
               Beginning of the advisory - removed AUSCERT advisory header
               to avoid confusion.
June 12, 1996  Updates section - added clarification concerning ROW access
               rights.








-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMiTCFnVP+x0t4w7BAQGCNgP9HzQyAGwZKH0XxXctYdq6u+9I9QPkAmG9
BGpgug7ZFcL1P+9R/M+1yOCMS4UScwiTeaeT3iMb0uCXeKmEUtC526uv7eLqb1RA
GeT8+BAkAym943l/LOpnfmUJgCWHKv39DZJXUym5SWM9BpDwPW+u0eIsoycNuhWZ
Qa5aDuncx7o=
=GFo0
-----END PGP SIGNATURE-----

