
-----BEGIN PGP SIGNED MESSAGE-----

=============================================================================
CERT(sm) Advisory CA-97.01
Original issue date: January 6, 1997
Last revised: January 22, 1997 - Updates - Added SGI and Sun patch
              information.

Topic: Multi-platform Unix FLEXlm Vulnerabilities

- -----------------------------------------------------------------------------

   The text of this advisory was originally released on September 19, 1996, as
   AUSCERT Advisory AA-96.03 "Multi-platform Unix FLEXlm Vulnerabilities,"
   developed by Australian Computer Emergency Response Team. Because of the
   seriousness of the problem, we are reprinting the AUSCERT advisory here
   with their permission. Only the contact information at the end has changed:
   AUSCERT contact information has been replaced with CERT/CC contact
   information.

   We will update this advisory as we receive additional information.
   Look for it in an "Updates" section at the end of the advisory.

==============================================================================

AUSCERT has received information concerning several problems involving
the use of the FLEXlm licence management package on Unix systems.  FLEXlm
is used by many vendors to licence their products, and is supplied to them
by GLOBEtrotter Software (previously, it was supplied by Highland
Software).  Many vendors have misconfigured the FLEXlm system to run as
the root user, and some versions of the FLEXlm licence management daemon
lmgrd contain a security vulnerability.  These problems may allow local
users to create arbitrary files on the system and execute arbitrary
programs using the privileges of the user running the FLEXlm daemons.

System administrators are advised that the FLEXlm package may be installed
as part of the installation procedures of other vendor and third-party
products.  Due to the way that the licence management software is often
installed, it may be unnecessarily running as root making it possible to
gain unauthorised privileged access.

This means that the FLEXlm package may be installed on systems and running
as the root user without the knowledge of the system administrator.

Note that the vulnerabilities described here do not affect the security of
the FLEXlm licences and licencing restriction.  The vulnerabilities allow
users to compromise security of the Operating System.

- ----------------------------------------------------------------------------

1.  Description

The FLEXlm licence management package is used by many vendors to licence
their products.  Many vendors have misconfigured the FLEXlm system to run
as the root user which opens a number of computer security vulnerabilities
which can be used to compromise the Unix operating system.  This is
described in paragraph (a).

In addition, some versions of the FLEXlm licence management daemon lmgrd
contain a security vulnerability.  This is described in paragraph (b).

(a) Insecure configuration of vendor product installation

    Due to some confusion in the documentation supplied to vendors using
    the FLEXlm package, the FLEXlm licence management software often runs
    with root privileges.  This often occurs due to the FLEXlm daemons
    being started by the system initialisation scripts.  If the daemons
    are running with root privileges they may be used by local users to
    gain unauthorised root privileges.  This potentially affects all
    versions of the FLEXlm licence management daemon.

    GLOBEtrotter Software advise that the FLEXlm package does not require
    root privileges to operate correctly.  FLEXlm daemons should be started
    by a non-privileged user with a restrictive umask setting, limiting
    the associated configuration vulnerabilities.

(b) Security vulnerability in FLEXlm licence management daemon

    A vulnerability has been found in the FLEXlm licence management daemon
    which may allow local users unauthorised access to the account running
    the FLEXlm licence management daemon.

    This vulnerability exists in all versions of the FLEXlm licence
    management daemon from version 4.0 up to, and including, version 5.0a.
    A new version of the daemon has been made available by GLOBEtrotter
    Software that fixes this vulnerability.  See Section 3.4.

    Versions earlier than version 4.0 do not have this vulnerability.
    GLOBEtrotter Software advise that all existing versions of the lmgrd
    daemon may be updated to the most recent version (version 5.0b) without
    change in functionality.  This version of lmgrd will work successfully
    with all existing FLEXlm-licensed products.  See Section 3.4.

1.1 Additional Description Information

This section contains additional information on locating any FLEXlm
components, determining the configuration of those components, and
identifying information required for the Workarounds/Solutions in
Section 3.

(a) Vendor configurations may be customised

    Vendors using the FLEXlm licence management package to licence their
    products have the ability to customise FLEXlm to meet their own needs.
    This may include names, locations, and content of many files, in
    addition to how the software is installed and used.  Therefore, care
    is required in locating any vulnerable software or configurations,
    and implementing workaround solutions.

(b) Determining if FLEXlm is installed

    The FLEXlm licence management package is often installed as part of
    the installation procedures of other vendor and third-party products.
    The system administrator may not be aware that FLEXlm has been
    installed.

    The following command run as root should determine if the FLEXlm
    licence management software is installed.

    # find /etc -type f -exec egrep -il 'lmgrd|flexlm|licdir' {} \;

    Any files listed should be investigated further to see if they relate
    to the FLEXlm licence management product.

    In particular, it is important to locate the FLEXlm licence management
    initialisation files (the files where FLEXlm licence management daemons
    are started from) as these will become important when discussing the
    Workarounds/Solutions in Section 3.

(c) Determining the version of the FLEXlm licence management daemon(s)

    The version of the FLEXlm licence management daemon can be determined
    by examining the strings(1) output of the binary daemon and searching
    for the strings "Copyright" and "FLEXlm".  For example:

# strings /usr/local/flexlm/licences/lmgrd | grep -i copyright | grep -i
flexlm

    Note that more than one version of the FLEXlm licence management
    daemon may be executing, depending on what products are installed.

    The version number is also written to stdout (which may have been
    redirected to a log file) when the licence management daemon is
    started.

(d) Identifying the user running the FLEXlm licence management daemons

    The licence management daemon is often called "lmgrd" or some
    derivative containing the string "lmgrd" (for example, lmgrd.abc).
    On some products, the name of the licence management daemon may have
    been changed to an arbitrary name (for example, lm_ABC4.ld).  It should
    be possible to locate most running versions of the licence management
    daemon by examining the files identified in Section 1.1(b) or by using
    one of the following commands (Note this may locate other processes
    not related to FLEXlm, and may not locate all FLEXlm related
    processes):

        % ps -auxww | grep -i lm | grep -v grep         # BSD systems
        % ps -ef | grep -i lm | grep -v grep            # SYS V systems

    If any licence management daemon is running as the root user, then a
    number of vulnerabilities exist as the daemon was not designed to be
    run with root privileges.

    Note that more than one FLEXlm licence management daemon may be running
    depending on what products have been installed.  It is important to
    check for all running versions of the daemon.

(e) Locating the licence management files

    Each licence management daemon has an associated licence file.  The
    licence file is usually specified by the "-c" option on the command
    line, the LM_LICENSE_FILE environment variable, or is found in the
    default location /usr/local/flexlm/licenses/license.dat.  The licence
    file describes which products the daemon is administering and the
    location of associated daemons.  The licence files become important
    when discussing the Workarounds/Solutions in Section 3.

2.  Impact

    Any versions of the FLEXlm licence management daemons executing using
    a system account (for example, bin, daemon, sys) or a privileged
    account (such as root) may allow local users to create or overwrite
    arbitrary files on the system.  This may be leveraged to gain root
    access.

    FLEXlm licence management daemons containing the security vulnerability
    (indicated in Section 1(b)) may allow local users unauthorised access
    to the account running the daemons.

    Information on gaining unauthorised access to Unix systems using the
    FLEXlm Licence Management software has been widely distributed.

3.  Workarounds/Solution

Note that all four (4) sections should be reviewed and implemented if
appropriate.  Each section addresses a different problem.

After the installation of ANY product or upgrade, the system must be
checked to verify if a FLEXlm licence management daemon has been added.
If a FLEXlm licence management daemon has been added, then Sections 3.1
to 3.4 of this Advisory should be applied to it to ensure a more secure
configuration.

3.1 Run as a non-privileged user

    GLOBEtrotter Software advise that the FLEXlm licence management
    software does not require root privileges to operate.  The FLEXlm
    licence management daemon should be run by a non-privileged user.

    If the licence management daemon is executing with root or some other
    system account permissions (such as bin, sys, daemon or any other
    system account), it must be modified to use a non-privileged user.

    If the licence management daemon is already executing as a
    non-privileged user, then the remainder of Section 3.1 may be skipped.

    It is recommended that a new user "flexlm" be created for the specific
    purpose of running the FLEXlm licence management daemon.  In this
    case, Steps 3.1.1 through 3.1.5 should be followed.

    3.1.1 Create a non-privileged account for use by FLEXlm.  For example:

        flexlm:*:2000:250:FLEXlm Licence Manager:/nonexistent:/bin/sh

          Note the account must have the following properties:
            . password set to '*' as interactive access is not required
            . a unique userid (the 2000 is only an example)
            . a unique groupid (the 250 is only an example)
            . a shell of /bin/sh

          The following instructions refer to this account as the "flexlm
          user".  If the FLEXlm daemons were already running as a
          non-privileged user, then this will be the "flexlm user" below.

    3.1.2 Locate the licence file(s).  These may be identified in one of
          three ways:
            . specified by the "-c" option to the FLEXlm licence daemons
            . specified by the LM_LICENSE_FILE environment variable
            . located in the default location:
                        /usr/local/flexlm/licenses/license.dat
          Note that there is always a single licence file for each licence
          daemon, but there may be more than one licence daemon running
          on a system.

    3.1.3 The licence management daemons must use a non-privileged TCP
          port for communication.  The port number chosen may be arbitrary,
          but all clients must be configured to use the same port.

          The port is specified in the licence data file on the SERVER
          line.  It is the fourth (4th) field on this line.  For example:

                SERVER xyzzy 123456789 1234

          the port number is 1234.

    3.1.4 Locate where the FLEXlm licence management daemon is started.
          This is often in the system startup scripts, but may not
          exclusively be so.  An example startup line is:

        $licdir/$lmgrd -c $licdir/$licfile >> /tmp/license_log 2>&1 &

          Logging information is written to stdout by the daemons, and is
          often redirected to a log file when the daemon is started.

    3.1.5 Modify the line in the FLEXlm startup files that starts the
          licence management daemon to look similar to the following:

su flexlm -c "{original command line in startup file}"

          where flexlm is the user created in Step 3.1.1.  Note that the
          logging information that is written to stdout from the daemon
          should not be written to files in /tmp or other world writable
          directories, but to a specially created directory that the flexlm
          user can write log information to.

          For example:

su flexlm -c "$licdir/$lmgrd -c ... >> /var/log/flexlm/license_log 2>&1 &"

3.2 File Ownership

    Regardless of which user is executing the FLEXlm licence management
    software, additional security vulnerabilities may allow a user to gain
    unauthorised access to the account running the daemon or engage in
    denial of service attacks by deleting files.

    These vulnerabilities may be limited if you ensure that no files on
    the system are owned or are writable by the flexlm user.  The possible
    exception to this requirement is log files (see Section 3.1.4) and
    temporary files.  All licence and FLEXlm executable files must be
    readable or executable by the flexlm user.  Additional daemons required
    by the FLEXlm licence management daemon are specified in the licence
    data files (located in Section 3.1.2) on the DAEMON line.

    These file ownership and mode changes should be done for all versions
    of FLEXlm.

    Note that some vendors may have installed the FLEXlm software owned
    by the flexlm user.  This configuration should be modified as detailed
    in this section.

3.3 umask Setting

    The FLEXlm licence management daemons inherit the umask setting from
    the environment in which they are started.  When FLEXlm is started as
    part of the system initialisation procedures, the umask is inherited from
    init(1M) and is usually set to 000.  This means that FLEXlm will
    open files which are world and group writable.  A more appropriate
    umask setting is 022.

    This should be done for all versions of FLEXlm.

    The umask can be set in the FLEXlm startup files which were identified
    in Section 3.1.4.  This should be the first command executed in the
    startup script for FLEXlm licence management daemons.

    For example:

                #!/bin/sh
                umask 022                       # add this line here
                <rest of the FLEXlm startup file>

3.4 Vendor Patch for Vulnerability

    GLOBEtrotter Software have made a new version of the FLEXlm licence
    management daemon (version 5.0b) available which rectifies the reported
    vulnerability in Section 1(b).

    All versions of the FLEXlm licence management daemon from version 4.0
    up to, and including, version 5.0a should be upgraded immediately.

    GLOBEtrotter Software advise that all versions of the FLEXlm lmgrd
    may be upgraded to the latest version (version 5.0b) without loss of
    existing functionality.  This version of lmgrd will work successfully
    with all existing FLEXlm-licensed products.

    Note that there may be more than one copy of FLEXlm's lmgrd on your
    system that requires upgrading, depending on what products are
    installed.  The existing licence management daemon(s) should be
    replaced with the new version, but the location and file name of the
    version you are replacing should be preserved.

    Version 5.0b of the FLEXlm licence management daemon may be found at

                http://www.globetrotter.com/lmgrd.htm

        MD5 (alpha_u1/lmgrd) = 40ec89f3c9cfcdecfaa442d59db179e1
        MD5 (decs_u4/lmgrd) = 0cd60373d0f0bef8f7a2de290306490b
        MD5 (hal_u5/lmgrd) = 1e678c62d6346480c6ce097df1a6c708
        MD5 (hp300_u8/lmgrd) = ffbdf1c581fd383ca01ba239230f2964
        MD5 (hp700_u8/lmgrd) = f972b3a449cd57e8d472a0394613e076
        MD5 (i86_d4/lmgrd) = 37256e1abe50116c504b6d2f83a23c55
        MD5 (i86_l1/lmgrd) = f1bbfdf13d1145fb3b18afb063b93ac3
        MD5 (i86_x5/lmgrd) = e6623c2124205512fc9ed21bc9aee061
        MD5 (ncr_u2/lmgrd) = 0919251ca4321dfaa166e008f8d34899
        MD5 (nec_u2/lmgrd) = 7e1ae2664219f59e0c26b1a1d97838df
        MD5 (ppc_u4/lmgrd) = d4d038cd5bdfa4c44d2523cf11461d63
        MD5 (ppc_x5/lmgrd) = f1aae597d4052734b4e01cac76407cf6
        MD5 (rm400_u5/lmgrd) = cb2d48efa809cbb3457f835f2db47926
        MD5 (rs6000_u3/lmgrd) = fadf0fc424f1fcc11cd04fe8678b79cf
        MD5 (sco_u3/lmgrd) = e288917fb8fac8fdc8f1f2a9d985eb50
        MD5 (sgi_u4/lmgrd) = 0637f1dae3adb5d7a3597b6d486e18af
        MD5 (sgi_u5/lmgrd) = 31f1f1d1b02917f4c9c062c33e4636a4
        MD5 (sgir8_u6/lmgrd) = ba0892403ef4bebf38ad22831d3d8183
        MD5 (sony_u4/lmgrd) = 032b4521333e7583afd0f783f5555522
        MD5 (sun4_u4/lmgrd) = f87130d077d4d1cc8469d9818a085d33
        MD5 (sun4_u5/lmgrd) = 36a2930f3dcbe92155866e7a9864b8a5

    A copy of these files will be available until 31-Oct-1996 from:

        ftp://ftp.auscert.org.au/pub/mirrors/ftp.globetrotter.com/flexlm/unix/

4.  Additional information

4.1 User Manual and Frequently Asked Questions

    GLOBEtrotter Software have a user manual that describes the FLEXlm
    Licence Management system which is available to all users.  A FAQ
    (Frequently Asked Questions) document containing useful information
    is also available.  These can be located at:

                http://www.globetrotter.com/manual.htm
                http://www.globetrotter.com/faq.htm

4.2 Additional Vendor Information

    GLOBEtrotter Software have made available some additional information
    concerning these security vulnerabilities.  It can be accessed at:

                http://www.globetrotter.com/auscert.htm

4.3 General misconfiguration description

    The misconfiguration of the FLEXlm licence management daemon is a
    generic problem where software that was not designed to be run with
    root privileges automatically gains those privileges as a result of
    being started by the system initialisation scripts.  Only those
    programs that require root privileges should be run as root.

    Attention is drawn to the Unix Secure Programming Checklist which
    addresses this issue, in addition to others.  The checklist is
    available from:

ftp://ftp.auscert.org.au/pub/auscert/papers/secure_programming_checklist

- ----------------------------------------------------------------------------
AUSCERT thanks Peter Marelas from The Fulcrum Consulting Group,
GLOBEtrotter Software, DFN-CERT, CERT/CC, and Sun Microsystems for their
advice and cooperation in this matter.
- ----------------------------------------------------------------------------

- ------------------------------------------------------------------------------

If you believe that your system has been compromised, contact the CERT
Coordination Center or your representative in the Forum of Incident
Response and Security Teams (FIRST).

We strongly urge you to encrypt any sensitive information you send by email.
The CERT Coordination Center can support a shared DES key and PGP. Contact
the CERT staff for more information.

Location of CERT PGP key
         ftp://info.cert.org/pub/CERT_PGP.key

CERT Contact Information
- ------------------------
Email    cert@cert.org

Phone    +1 412-268-7090 (24-hour hotline)
                CERT personnel answer 8:30-5:00 p.m. EST
                (GMT-5)/EDT(GMT-4), and are on call for
                emergencies during other hours.

Fax      +1 412-268-6989

Postal address
        CERT Coordination Center
        Software Engineering Institute
        Carnegie Mellon University
        Pittsburgh PA 15213-3890
        USA

CERT publications, information about FIRST representatives, and other
security-related information are available for anonymous FTP from
        http://www.cert.org/
        ftp://info.cert.org/pub/

CERT advisories and bulletins are also posted on the USENET newsgroup
        comp.security.announce

To be added to our mailing list for CERT advisories and bulletins, send your
email address to
        cert-advisory-request@cert.org


CERT is a service mark of Carnegie Mellon University.

This file: ftp://info.cert.org/pub/cert_advisories/CA-97.01.flex_lm
           http://www.cert.org
               click on "CERT Advisories"

=============================================================================
UPDATES

Silicon Graphics, Inc.
======================

The solution to this problem is to install version 3.0 of the the License
Tools, license_eoe subsystem.

To determine the version of License Tools installed on a particular
system, the following command can be used:


   % versions license_eoe

   I = Installed, R = Removed

   Name                 Date      Description

   I  license_eoe          02/13/96  License Tools 1.0
   I  license_eoe.man      02/13/96  License Tools 1.0 Manual Pages
   I  license_eoe.man.license_eoe  02/13/96  License Tools 1.0 Manual Pages
   I  license_eoe.man.relnotes  02/13/96  License Tools 1.0 Release Notes
   I  license_eoe.sw       02/13/96  License Tools 1.0 Software
   I  license_eoe.sw.license_eoe  02/13/96  License Tools 1.0 Software


In the above case, version 1.0 of the License Tools is installed and the
steps below should be performed.  If the output returned indicates
"License Tools 3.0," the latest license subsystem is installed and no
further action is required.

**** IRIX 4.x ****

The 4.x version of IRIX is not vulnerable as no license manager
subsystems were released for this IRIX version.  No action is
required.

**** IRIX 5.0.x, 5.1.x, 5.2 ****

The 5.0.x, 5.1.x and 5.2 versions of IRIX are not vulnerable as no
license manager subsystems were released for these IRIX versions.
No action is required.

**** IRIX 5.3 ****

For the IRIX operating system version 5.3 an inst-able new version
of software has been generated and made available via anonymous FTP
and your service/support provider.  The software is version 3.0 of
the License Tools, license_eoe subsystem and will install on IRIX 5.3
only.

The SGI anonymous FTP site is sgigate.sgi.com (204.94.209.1) or its
mirror, ftp.sgi.com.   Software is referred to as License5.3.tar and
can be found in the following directories on the FTP server:

        ~ftp/Security

                or

        ~ftp/Patches/5.3

                        ##### Checksums ####

The actual software will be a tar file containing the following files:


Filename:                 license_eoe
Algorithm #1 (sum -r):    01409 7 license_eoe
Algorithm #2 (sum):       56955 7 license_eoe
MD5 checksum:             38232F3DE67373875577B167B2DA2DA3

Filename:                 license_eoe.books
Algorithm #1 (sum -r):    33405 809 license_eoe.books
Algorithm #2 (sum):       53177 809 license_eoe.books
MD5 checksum:             D1D931936AB681A7B259BD75DCA6D7F9

Filename:                 license_eoe.idb
Algorithm #1 (sum -r):    59742 54 license_eoe.idb
Algorithm #2 (sum):       32839 54 license_eoe.idb
MD5 checksum:             4F7EE6965539FCFEEDE07E3FFD71CF5A


Filename:                 license_eoe.man
Algorithm #1 (sum -r):    58166 271 license_eoe.man
Algorithm #2 (sum):       23426 271 license_eoe.man
MD5 checksum:             41946D8E27032A929350B2C27D065DE5

Filename:                 license_eoe.sw
Algorithm #1 (sum -r):    29827 7692 license_eoe.sw
Algorithm #2 (sum):       52617 7692 license_eoe.sw
MD5 checksum:             720EF1907DD0C3113CB4A98AD602010B


**** IRIX 6.0, 6.0.1 *****

The 6.0.x versions of IRIX are not vulnerable as no license manager
subsystems were released for these IRIX versions.  No action is required.


**** IRIX 6.1 ****

The license manager software provided with IRIX 6.1 is version
1.0 of the License Tools, license_eoe subsystem for IRIX 6.1.   This
version is not vulnerable to these security issues.

However, if an upgrade of the License Tools, license_eoe subsystem
was done (see above section on determining version installed with
versions command), then a security vulnerability might exist.
In order to remove this vulnerability, either a downgrade to
version 1.0 of the License Tools, license_eoe subsystem is
required or upgrade the entire IRIX version to 6.2 and apply
the version 3.0 of the License Tools, license_eoe subsystem.

**** IRIX 6.2 ****

For the IRIX operating system version 6.2 an inst-able new version
of software has been generated and made available via anonymous FTP
and your service/support provider.  The software is version 3.0 of
the License Tools, license_eoe subsystem and will install on IRIX 6.2
only.

The SGI anonymous FTP site is sgigate.sgi.com (204.94.209.1) or its
mirror, ftp.sgi.com.   Software is referred to as License6.2.tar and
can be found in the following directories on the FTP server:

        ~ftp/Security

                or

        ~ftp/Patches/6.2

                        ##### Checksums ####

The actual software will be a tar file containing the following files:


Filename:                 license_eoe
Algorithm #1 (sum -r):    53638 7 license_eoe
Algorithm #2 (sum):       7547 7 license_eoe
MD5 checksum:             05A65EE03BEE71A464D4B7AB9962F228

Filename:                 license_eoe.books
Algorithm #1 (sum -r):    03494 907 license_eoe.books
Algorithm #2 (sum):       25664 907 license_eoe.books
MD5 checksum:             AE86ED7D3C36F67C2505C06C41FCD174

Filename:                 license_eoe.idb
Algorithm #1 (sum -r):    15441 58 license_eoe.idb
Algorithm #2 (sum):       59702 58 license_eoe.idb
MD5 checksum:             811CD48FA5BD57E79B4D36839185EED9

Filename:                 license_eoe.man
Algorithm #1 (sum -r):    63961 271 license_eoe.man
Algorithm #2 (sum):       25496 271 license_eoe.man
MD5 checksum:             3086F992150A673C5110CCC16E20CA96

Filename:                 license_eoe.sw
Algorithm #1 (sum -r):    05953 7483 license_eoe.sw
Algorithm #2 (sum):       33599 7483 license_eoe.sw
MD5 checksum:             BE52C7C2CCDAB2B491F6FA0412E4A66D

**** IRIX 6.3 ****

The license manager softwares provided with this version of
IRIX are not vulnerable to these security issues.



Sun Microsystems, Inc.
======================

The following patches are now available from Sun.

Patch-ID# 104174-01
Keywords: CERT security license FLEXlm
Synopsis: FLEXlm Licensing (SUNWlicsw, SUNWlit): CERT security advisory patch
Date: Jan/13/97

Solaris Release: 2.4 2.5

SunOS Release: 5.4 5.5

Patch-ID# 104186-01
Keywords: CERT security license FLEXlm
Synopsis: FLEXlm (SUNWlicsw, SUNWlit): CERT security advisory patch
Date: Jan/13/97

Solaris Release: 2.4_x86 2.5_x86 2.5.1_x86

SunOS Release: 5.4_x86 5.5_x86 5.5.1_x86

Patch-ID# 104217-01
Keywords: CERT security license FLEXlm
Synopsis: FLEXlm (SUNWlicsw, SUNWlit) 4.1: CERT security advisory patch
Date: Jan/13/97

Solaris Release: 1.1.1 1.1.2 1.1.3 1.1.3_U1 1.1.4 1.1.4-JL

SunOS Release: 4.1.1 4.1.2 4.1.3 4.1.3_U1 4.1.4 4.1.4-JL




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Revision history

Jan. 22, 1997 - Updates - Added SGI and Sun patch information.

-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMuY/o3VP+x0t4w7BAQFKFQQA0p3+FGOk18RNb5d9eySkStLuI4CGaGi4
lzZxxGIv1IDwbMX3TNFF6fNW1CpSV8xarfs2ZMloMGeNYUWww9R9MZxnxw4jcO+C
5o3chU45cgn5JzsYqOPMKf6AfIl1v7yytWEJrJSfuKv1/GR7i3lhn2HIWJipCYbd
ptblK3sUKv8=
=xleO
-----END PGP SIGNATURE-----

