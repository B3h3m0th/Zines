
-----BEGIN PGP SIGNED MESSAGE-----

=============================================================================
CERT(sm) Advisory CA-96.11
Original issue date: May 29, 1996
Last revised: August 30, 1996
              Removed references to the advisory README file.

              A complete revision history is at the end of this file.

Topic: Interpreters in CGI bin Directories
- -----------------------------------------------------------------------------

Many sites that maintain a Web server support CGI programs. Often these
programs are scripts that are run by general-purpose interpreters, such as
/bin/sh or PERL. If the interpreters are located in the CGI bin directory
along with the associated scripts, intruders can access the interpreters
directly and arrange to execute arbitrary commands on the Web server system.
This problem has been widely discussed in several forums. Unfortunately, some
sites have not corrected it.

The CERT Coordination Center recommends that you never put interpreters in a
Web server's CGI bin directory.

We will update this advisory as we receive additional information.
Please check advisory files regularly for updates that relate to your site.

- -----------------------------------------------------------------------------
I.   Description

     To execute CGI scripts, a Web server must be able to access the
     interpreter used for that script. Early documentation for Netscape and
     other servers recommended placing the interpreters in the CGI bin
     directory to ensure that they were available to run the script.

     All programs in the CGI bin directory can be executed with arbitrary
     arguments, so it is important to carefully design the programs
     to permit only the intended actions regardless of what arguments are
     used. This is difficult enough in general, but is a special problem for
     general-purpose interpreters since they are designed to execute
     arbitrary programs based on their arguments. *All* programs in the
     CGI bin directory must be evaluated carefully, even relatively
     limited programs such as gnu-tar and find.

     Note that the directory for CGI programs is typically called "cgi-bin"
     but the server may be configured to use a different name.

II.  Impact

     If general-purpose interpreters are accessible in a Web server's CGI bin
     directory, then a remote user can execute any command the interpreters
     can execute on that server.

III. Solution

     The solution to this problem is to ensure that the CGI bin directory
     does not include any general-purpose interpreters, for example
             + PERL
             + Tcl
             + UNIX shells (sh, csh, ksh, etc.)

     A variety of methods can be used to safely install such interpreters;
     methods vary depending on the system and Web server involved.

     On Unix systems, the location of the interpreter is given on the first
     line of the script:
        #! /path/to/interpreter

     On other systems, such as NT, there is an association between filename
     extensions and the applications used to run them. If your Web server
     uses this association, you can give CGI scripts an appropriate suffix
     (for example, ".pl" for PERL), which is registered to the appropriate
     interpreter. This avoids the need to install the interpreter in the
     CGI bin directory, thus avoiding the problem.

     Check with your Web server vendor for specific information.

     Netscape reports that the 2.0 versions of their FastTrack and Enterprise
     Servers, (both the current Beta and upcoming final versions), do support
     file interpreter associations.

     Further reading:

        Tom Christiansen has a Web page with details about this problem
        and a script that can be used to test for it:
                http://perl.com/perl/news/latro-announce.html

         Lincoln Stein's WWW Security FAQ includes a section on "Problems
         with Specific Servers," which discusses this and related problems:
                http://www.genome.wi.mit.edu/WWW/faqs/www-security-faq.html

- ---------------------------------------------------------------------------
The CERT Coordination Center thanks Lincoln Stein, Tom Christiansen, and the
members of AUSCERT and DFN-CERT for their contributions to the information in
this advisory.
- ---------------------------------------------------------------------------

If you believe that your system has been compromised, contact the CERT
Coordination Center or your representative in the Forum of Incident
Response and Security Teams (FIRST).

We strongly urge you to encrypt any sensitive information you send by email.
The CERT Coordination Center can support a shared DES key and PGP. Contact
the CERT staff for more information.

Location of CERT PGP key
         ftp://info.cert.org/pub/CERT_PGP.key

CERT Contact Information
- ------------------------
Email    cert@cert.org

Phone    +1 412-268-7090 (24-hour hotline)
                CERT personnel answer 8:30-5:00 p.m. EST
                (GMT-5)/EDT(GMT-4), and are on call for
                emergencies during other hours.

Fax      +1 412-268-6989

Postal address
        CERT Coordination Center
        Software Engineering Institute
        Carnegie Mellon University
        Pittsburgh PA 15213-3890
        USA

CERT publications, information about FIRST representatives, and other
security-related information are available for anonymous FTP from
        http://www.cert.org/
        ftp://info.cert.org/pub/

CERT advisories and bulletins are also posted on the USENET newsgroup
        comp.security.announce

To be added to our mailing list for CERT advisories and bulletins, send your
email address to
        cert-advisory-request@cert.org


Copyright 1996 Carnegie Mellon University
This material may be reproduced and distributed without permission provided
it is used for noncommercial purposes and the copyright statement is
included.

CERT is a service mark of Carnegie Mellon University.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Revision history

Aug. 30, 1996  Removed references to CA-96.11.README.





-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMiTEznVP+x0t4w7BAQGUSgQAkyk5kZXkVOecd1wH75prQsBJVTiduRdF
d60VgCwjaR79/PwmxBBijC8oB42xuPrBNyuCfQm00l4+US7hV25b91J3aQO3obmr
eRL+dZnU9cGzk9sD3V49W7wweWroANYVN60jF2uPEq4uHsnggyjGZDVS1RCo/9iL
5R1UX3M965Y=
=FgjX
-----END PGP SIGNATURE-----

