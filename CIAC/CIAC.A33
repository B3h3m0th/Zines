

        _____________________________________________________

              The Computer Incident Advisory Capability

                         ___  __ __    _     ___

                        /       |     / \   /

                        \___  __|__  /___\  \___

        _____________________________________________________

                        Informational Bulletin



             Virus Propagation in Novelle and Other Networks    

        

September 21, 1990, 1000 PST                               Number A-33



Problem:  Virus propagation on write-protected file systems 

Types: Many known viruses, most frequently variants of the Jerusalem

(Israeli) virus 

Platform: MS-DOS computers 

Damage: Files that use software write-protection schemes cannot be

assumed safe from damage due to virus infection 

Symptoms: Virus infection on write-protected files

Detection:  VIRHUNT, RESSCAN, CodeSafe, Vi-Spy, IBM Scan, FPROT

Eradication: VIRHUNT, CodeSafe, FPROT, and others (see text in p. 2 of

this bulletin for recommended procedures)



                 Critical Virus Propagation Facts 

 



This bulletin is to warn of a virus threat to networks for MS-DOS

systems.  File servers (e.g., Novell file servers) use attribute bits

to perform write protection on files stored on server machines.  Many

viruses will clear these attribute protection bits before they attempt

infection, thus circumventing the write protection scheme.  Thus,

write-protecting a program does not guarantee that the file is not

infected with the virus.



The following is a common scenario reported to CIAC:  a floppy infected

with the Jerusalem-B virus is inserted into a user's PC attached to a

Novell network.  Once this virus is executed, it resides in the PC's

memory.  When the user attempts to logon to the file server (running

the program login.exe), the virus infects this program, even though the

program is write-protected.  Login.exe is a shared program that is

executed by each user as s/he connects to the Novell network.  Thus,

each time a user logs in to the network, his/her machine immediately

becomes infected with the Jerusalem-B virus.  The network allows the

Jerusalem-B virus to spread considerably more quickly than if it had

spread through exchange of floppy disks.



When someone disinfects a system of PCs or PC clones on a Novell or

similar file system,   CIAC recommends the following procedures:



1)      Detect the virus using one of the recommended packages for

detecting and identifying the virus.  Determine exactly which virus has

infected the system, and that all virus types have been detected.

Contact CIAC if you need assistance.



2)      Deactivate the network connecting the PCs/PC clones together.

This includes shutting down the file servers and unmounting the

partitions from the users' PCs/PC clones.



3)      Disinfect the server machines using an anti-virus package known

to be effective against the detected virus.  Alternately, reformat the

server disks and re-install the system from original diskettes, then

restore the data files from a recent backup.  Do not attempt to restore

programs (i.e., executable files) from a backup, as this is likely to

reinfect your system.



4)      Disinfect each user's PC/PC clone using the same procedure as

in step 2.



5)      Verify that the virus does not reside on the file server or any

user's PC/PC clone.



6)      Bring the network file system back up.



For additional information or assistance, please contact CIAC: 



        Tom Longstaff

        (415) 423-4416 or (FTS) 543-4416

        FAX:  (415) 423-0913 or (FTS) 543-0913



 Send e-mail to:



        ciac@tiger.llnl.gov



Neither the United States Government nor the University of California

nor any of their employees, makes any warranty,  expressed or implied,

or assumes any legal liability or responsibility for the accuracy,

completeness, or usefulness of any information, product, or process

disclosed, or represents that its use would not infringe privately

owned rights.  Reference herein to any specific commercial products,

process, or service by trade name, trademark manufacturer, or

otherwise, does not necessarily constitute or imply its endorsement,

recommendation, or favoring by the United States Government or the

University of California.  The views and opinions of authors expressed

herein do not necessarily state or reflect those of the United States

Government nor the University of California, and shall not be used for

advertising or product endorsement purposes.


