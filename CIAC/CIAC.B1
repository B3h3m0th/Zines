

        _____________________________________________________

              The Computer Incident Advisory Capability

                         ___  __ __    _     ___

                        /       |     / \   /

                        \___  __|__  /___\  \___

        _____________________________________________________

                        Informational Bulletin                                 



             Security Problem on the NeXT Operating System 



October 5, 1990, 0800 PST                                  Number B-1



CIAC has been informed of a series of security problems in the NeXT

operating system that are now becoming well known.  If your site

operates NeXT machines, CIAC recommends that you follow the procedures

below to secure these systems against attack.  The information

contained in this message has been provided by David Besemer, NeXT

Computer, Inc. and from the Computer Emergency Response Team (CERT).

The following describes four potential security problems and NeXT

Computer's recommended solutions and known system impact.



Problem 1.  DESCRIPTION:  On Release 1.0 and 1.0a, a script exists in

/usr/etc/restore0.9 that is a setuid shell script.  The existence of

this script is a potential security problem.



IMPACT:  The script is only needed during the installation process and

isn't needed for normal usage.  It is possible for any logged in user

to gain root access.



SOLUTION:  NeXT owners running Release 1.0 or 1.0a should remove the

file /usr/etc/restore0.9 from all disks.  This file is installed by the

"BuildDisk" application, so it should be removed from all systems built

with the standard release disk, as well as from the standard release

disk itself (which will prevent the file from being installed on

systems built with the standard release disk in the future).  You must

be root to remove this script, and the command that will remove the

script is the following:



        # /bin/rm /usr/etc/restore0.9



Problem 2.  

DESCRIPTION:  On NeXT computers running Release 1.0 or 1.0a that also

have publicly accessible printers, users can gain  extra permissions

via a combination of bugs.



IMPACT: Computer intruders are able to exploit this security problem to

gain access to the system. Intruders, local users and remote users are

able to gain root access.



SOLUTION:  NeXT computer owners running Release 1.0 or 1.0a should do

two things to fix a potential security problem. First, the binary

/usr/lib/NextPrinter/npd must be replaced with a more secure version.

This more secure version of npd is available through your NeXT support

center.  You may contact this support center using electronic mail to:

ask_next@NeXT.com.  This patched npd is also available by anonymous FTP

on the nodes nova.cc.purdue.edu, umd5.umd.edu, and cs.orst.edu.  You

may also contact CIAC for help in obtaining this patch.  Upon receiving

a copy of the more secure npd, you must become root and install it in

place of the old one in /usr/lib/NextPrinter/npd.  The new npd binary

needs to be installed with the same permission bits (6755) and owner

(root) as the old npd binary.  The commands to install the new npd

binary are the following:



# /bin/mv /usr/lib/NextPrinter/npd /usr/lib/NextPrinter/npd.old 

# /bin/mv newnpd /usr/lib/NextPrinter/npd 



(In the above command, "newnpd" is the npd binary that you obtained

from your NeXT support center.)



# /etc/chown root /usr/lib/NextPrinter/npd 

# /etc/chmod 6755 /usr/lib/NextPrinter/npd

# /etc/chmod 440 /usr/lib/NextPrinter/npd.old 



The second half of the fix to this potential problem is to change the

permissions of directories on the system that are currently owned and

able to be written by group "wheel".  The command that will remove

write permission for directories owned and writable by group "wheel" is

below.  This command is all one line, and should be run as root.



# find / -group wheel ! -type l -perm -20 ! -perm -2 -ls -exec chmod

g-w {} \; -o -fstype nfs -prune





Problem 3.

DESCRIPTION: On NeXT computers running any release of the system

software, public access to the window server may be a potential

security problem.The default in Release 1.0 or 1.0a is correctly set so

that public access to the window server is not available.  It is

possible, when upgrading from a prior release, that the old

configuration files will be reused.  These old configuration files

could possibly enable public access to the window server.



IMPACT: This security problem will enable an intruder to gain access to

the system.



SOLUTION: If public access isn't needed, it should be disabled.



1. Launch the Preferences application, which is located in /NextApps 2.

Select the UNIX panel by pressing the button with the UNIX certificate

on it.



3. If the box next to Public Window Server contains a check, click on

the box to remove the check.



Problem 4.

DESCRIPTION: On NeXT computers running any release of the system

software, the "BuildDisk" application is executable by all users.



IMPACT: Allows a user to gain root access.



SOLUTION: Change the permissions on the "BuildDisk" application

allowing only root to execute it.  This can be accomplished with the

command:



# chmod 4700 /NextApps/BuildDisk



To remove "BuildDisk" from the default icon dock for new users, do the

following:



1. Create a new user account using the UserManager application. 

2. Log into the machine as that new user. 

3. Remove the BuildDisk application from the Application Dock by 

   dragging it out. 

4. Log out of the new account and log back in as root. 

5. Copy the file in ~newuser/.NeXT/.dock to 



   /usr/template/user/.NeXT/.dock



   (where ~newuser is the home directory of the new user account) 



6. Set the protections appropriately using the following command:  



        # chmod 555 /usr/template/user/.NeXT/.dock 



7. If you wish, with UserManager, remove the user account that you 

   created in step 1.



In release 2.0, the BuildDisk application will prompt for the root

password if it is run by a normal user.



NeXT has also reported that these potential problems have been fixed in

NeXT's Release 2.0, which will be available in November, 1990.  For

additional information or assistance, please contact CIAC or your NeXT

support center.



        Tom Longstaff

        (415) 423-4416 or (FTS) 543-4416

or

        David Brown

        (415) 423-9878 or (FTS) 543-9878



        FAX:  (415) 423-0913 or (FTS) 543-0913 

 

or send e-mail to:



        ciac@tiger.llnl.gov



Thanks to Corey Satten and Scott Dickson for discovering, documenting,

and helping resolve these problems.



Neither the United States Government nor the University of California

nor any of their employees, makes any warranty, expressed or implied,

or assumes any legal liability or responsibility for the accuracy,

completeness, or usefulness of any information, product, or process

disclosed, or represents that its use would not infringe privately

owned rights.  Reference herein to any specific commercial products,

process, or service by trade name, trademark manufacturer, or

otherwise, does not necessarily constitute or imply its endorsement,

recommendation, or favoring by the United States Government or the

University of California.  The views and opinions of authors expressed

herein do not necessarily state or reflect those of the United States

Government nor the University of California, and shall not be used for

advertising or product endorsement purposes.




