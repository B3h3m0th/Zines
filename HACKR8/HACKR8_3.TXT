
            -= H A C K E R S =-

                           Issue #8,  File #3 of 9

                             Checks and Balances

                 Courtesy of Voters Telecommunications Watch

===========================================================================
                               VTW BillWatch #62

          VTW BillWatch: A newsletter tracking US Federal legislation
         affecting civil liberties.  BillWatch is published about every
      week as long as Congress is in session. (Congress is out of session)

                   BillWatch is produced and published by the
                 Voters Telecommunications Watch (vtw@vtw.org)

                 Issue #62, Date: Tue Oct 22 02:32:41 EDT 1996

     Do not remove this banner.  See distribution instructions at the end.
___________________________________________________________________________
TABLE OF CONTENTS
    New feature at VTW: CaseWatch
    About VTW Center's CaseWatch
    Subscription Information and donation policy (unchanged 2/18/96)
___________________________________________________________________________
INTRODUCTION BY SHABBIR J. SAFDAR

I'm still not quite sure how we've done this, but I'm proud to say that
we still don't have any money and we're about to announce a brand new
Web publication.  Today, our nonprofit organization, the VTW Center
for Internet Education, unveils a new publication dedicated to following
important legal disputes that affect the net.

Our new publication, CaseWatch (http://www.vtwctr.org/casewatch/),
focuses on the evolving common law of the Internet.  Small, often
private disputes between individuals and organizations are today
creating the common law of the Internet, in at least as much as they
are a sign of society's continuing discourse on how to integrate the
Internet into our lives.  The disputes we cover in CaseWatch today may
continue to be obscure, but more than likely, we'll look back at them
many years from now with the benefit of perspective and see their
importance.

CaseWatch isn't for the faint of heart.  It assumes a knowledge of both
Internet technology and a not insignificant legal background.  However
it should still be accessible to most everyone reading this list, as you
are already better educated on these issues than most of the public.

CaseWatch is researched and produced by Diana Jarvis (daj@vtw.org).  Diana
graduated from Harvard in 1986, and Yale Law School in 1992. She clerked for
Justice Robert W. Sweet, a Federal District Judge in the southern district
of New York.  She has been with VTW since 1994 and is currently VTW Center's
Staff Counsel.

Please take a moment and visit CaseWatch at VTW Center's new home page
at http://www.vtwctr.org   Most of all, take a moment to send a note of
congratulations to Diana for her first VTW publication, as well as starting
work as VTW Center's first full time staff member.  She can be reached
at daj@vtw.org.

Here is this month's CaseWatch synopsis:


 What community standards should determine the obscenity of images
 loaded onto a Californian BBS and downloaded into Tennessee? We still
 don't know, thanks to the fact that Robert Thomas' habit of collecting
 addresses from all his customers enabled the Sixth Circuit to decide that
 the mere presence of a computer BBS did not make his operation any different
 from the traditional pornographer who sends magazines or videotapes
 through the mail. The Supreme Court apparently agrees, having denied
 certioriari on October 7th.


Don't forget!
On Sunday November 3, 1996 will be the seminar on the New York State
Internet Censorship legislation.  Be a part of it by showing up, or show
up online through RealAudio and participate in the trivia contest to win
valuable prizes (including VTW tshirts!).  For more information on the
seminar, see http://www.vtw.org/speech/

___________________________________________________________________________
ABOUT CASEWATCH

[The following information is taken from http://www.vtwctr.org/casewatch/about/]

The legislation tracked by VTW is not the only law in cyberspace. As
the Internet becomes more prevalent, it will inevitably become the
subject of disputes, some of which will be settled in court. Because
the law laid down by judges in their opinions resolving these disputes
-- the common law -- is just as much law as legislation passed by
Congress, CaseWatch will try to track court opinions on issues
affecting the online community much as BillWatch tracks legislation.

Chicago Law School professor Lawrence Lessig has argued for a chance to
allow the common law of cyberspace to developed unhindered by
unnecessary and premature legislation:

"[W]hat the system of cyberspace regulation will need is a way to pace
 any process of regulation -- a way to let the experience catch up with
 the technology .... This is the practice of the common law. [It] is
 democratic not because many people get to vote together on what the law
 should mean, but because many people get to say what the common law
 should mean, each ofter the other, in a temporally spaced dialogue of
 cases and jurisdiction. Unlike other lawmaking, what defines the
 process of the common law is small change, upon which much large change
 gets built; small understandings with which new understandings get
 made. What counsels it here is the way this process will function to
 create in an as yet uninhabited, unconstructed, world.

 What will be new are the communities that this space will allow, the
 constructive ... possibilities that these communities will bring.
 People meet, and talk, and live in cyberspace in ways not possible in
 real space. They build and define themselves in cyberspace in ways not
 possible in real space. And before they get cut apart by regulation, we
 should know something about their form, and more about their
 potential."

The Path of Cyberlaw, 104 Yale L.J. 1743, 1743-46 (1995).

Every new issue of Casewatch will try to bring you a new,  usually quite
recent case that will reflect the development of Lessig's common law of
cyberspace and highlight the ways in which the Internet is quickly
being drawn into our legal lexicon.

___________________________________________________________________________
SUBSCRIPTION INFORMATION AND DONATION POLICY

We do not accept unsolicited donations at this time.
If you want to help, for heaven's sakes, register
to vote at http://netvote96.mci.com/register.html.

You can receive BillWatch via email or WWW:

To subscribe via email, send mail to majordomo@vtw.org with
"subscribe vtw-announce" in the body of the message.  To
unsubscribe from BillWatch send mail to majordomo@vtw.org with
"unsubscribe vtw-announce" in the body of the message.

BillWatch can be found on the World Wide Web at http://www.vtw.org/billwatch/

___________________________________________________________________________
    End VTW BillWatch Issue #62, Date: Tue Oct 22 02:32:41 EDT 1996
___________________________________________________________________________
This file provided by:

      Voters Telecommunications Watch
    *** Watching out for your civil liberties ***

Email:      vtw@vtw.org (preferred)
Gopher:       gopher -p1/vtw gopher.panix.com
URL:   http://www.vtw.org/
Telephone:   (718) 596-2851 (last resort)

===========================================================================
                               VTW BillWatch #63

          VTW BillWatch: A newsletter tracking US Federal legislation
         affecting civil liberties.  BillWatch is published about every
      week as long as Congress is in session. (Congress is out of session)

                   BillWatch is produced and published by the
                 Voters Telecommunications Watch (vtw@vtw.org)

                 Issue #63, Date: Tue Oct 29 00:18:48 EST 1996

     Do not remove this banner.  See distribution instructions at the end.
___________________________________________________________________________
TABLE OF CONTENTS
    CaseWatch: Jurisdiction in cyberspace
    Subscription Information and donation policy (unchanged 2/18/96)
___________________________________________________________________________
CASEWATCH: JURISDICTION IN CYBERSPACE

[Here is a summary of this week's CaseWatch, http://www.vtwctr.org/casewatch/]

Dennis Toeppen is the guy who registered "Panavision.com"
(and then posted aerial photographs of Pana, Ill, on the site until NSI
put it on hold) along with approximately 240 other domain names, at least
some of them 'bitchin' corporate names'; as Josh Quittner put
it in his Wired column about mcdonalds.com.

By now at least four of the companies which have discovered their
companyname.com registered to Toeppen have now sued for assorted trademark
violations -- one of them being Panavision, of course -- and although the
real question is whether Panavision has a right to Panavision.com simply
because it has trademarked the name, the first question, as it turns out, is
whether Panavision (which is based in California) can sue Toeppen in a court
in the state of California, when the guy by his own admission rarely goes
there and maintains his web page in Illinois.

The court's analysis is this: it's not just having a web page
-- it's what you're doing with your web page which dictates where
you can get sued (and if you're infringing a trademark, you've had an effect
in the trademark owner's home town). But the details haven't all been worked
out yet. ....

[Read the whole story for free in this week's CaseWatch,
http://www.vtwctr.org/casewatch/]

___________________________________________________________________________
SUBSCRIPTION INFORMATION AND DONATION POLICY

We do not accept unsolicited donations at this time.
If you want to help, for heaven's sakes, register
to vote at http://netvote96.mci.com/register.html.

You can receive BillWatch via email or WWW:

To subscribe via email, send mail to majordomo@vtw.org with
"subscribe vtw-announce" in the body of the message.  To
unsubscribe from BillWatch send mail to majordomo@vtw.org with
"unsubscribe vtw-announce" in the body of the message.

BillWatch can be found on the World Wide Web at http://www.vtw.org/billwatch/

___________________________________________________________________________
    End VTW BillWatch Issue #63, Date: Tue Oct 29 00:18:48 EST 1996
___________________________________________________________________________
This file provided by:

      Voters Telecommunications Watch
    *** Watching out for your civil liberties ***

Email:      vtw@vtw.org (preferred)
Gopher:       gopher -p1/vtw gopher.panix.com
URL:   http://www.vtw.org/
Telephone:   (718) 596-2851 (last resort)
Copyright 1994-1996 (commercial use without permission prohibited)
===========================================================================

Copyright 1994-1996 (commercial use without permission prohibited)
===========================================================================

*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *
